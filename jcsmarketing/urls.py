#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
from django.conf.urls import url
from jcsmarketing import views


urlpatterns = [
    url(r'^jcs-home/$', views.index, name='index'),
    url(r'^compute/$', views.compute, name='compute'),
    url(r'^contactus/$', views.contactus, name='contactus'),
    url(r'^jcsdss/$', views.jcsdss, name='jcdss'),
    url(r'^jcsn/$', views.jcsn, name='jcsn'),
    url(r'^jcssbs/$', views.jcssbs, name='jcssbs'),
    url(r'^pricing/$', views.pricing, name='pricing'),
    url(r'^support/$', views.support, name='support'),
    url(r'^products/$', views.products, name='products'),
    url(r'^console/$', views.console, name='console'),
    url(r'^profilePage/$', views.profilepage, name='profilepage'),
    url(r'^updateName/$', views.updateName, name='updateName'),
    url(r'^updatePassword/$', views.updatePassword, name='updatePassword'),
    url(r'^updateEmail/$', views.updateEmail, name='updateEmail'),
]
