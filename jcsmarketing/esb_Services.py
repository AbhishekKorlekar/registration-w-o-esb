import requests as req
import json
from keystoneclient.auth.identity import v3 as identityV3
from keystoneclient import session
from neutronclient.neutron import client as neutronClient
import sys
import openstack_dashboard.settings as dashboard_settings

#update Details of the User

def updateName(EmailID,Name,phoneNumber):
	responseMessage = ""
	requestAccepted=False
	url = dashboard_settings.ESB_ENDPOINT + "/updateContact"
	print "the URL to invoke for updating contact is: ",url
	print "Name: ",Name
	print "EmailID",EmailID
	print "phoneNumber: ",phoneNumber
	try:
		requestBody = {
		       		"contactDetail": {
					"last_name": Name,
		            		"phone_mobile": phoneNumber,
  				        "email":EmailID,
		 		        "email_new":""
				}
		}
		headers = {'Content-type' : 'application/json','Accept' : 'text/plain'}
		res = req.put(url,data=json.dumps(requestBody),headers=headers)
		print "Afer sending the data to the server url is: ",res.url
		if res.status_code == 200:
                        requestAccepted=True
                        responseMessage = "UPDATE_SUCCESS"
                        print "responseMessage: ",responseMessage
                else:
                        requestAccepted=False
                        responseMessage = "UPDATE_FAILED"
                        print "responseMessage: ",responseMessage
                print "Response status", res.status_code
        except req.exceptions.RequestException as e:
                print e
                requestAccepted = False
                responseMsg = "ESB_SERVICE_ERROR"
        return responseMessage

def updatePassword(EmailID,Password):
        responseMessage = ""
        requestAccepted=False
        url = dashboard_settings.ESB_ENDPOINTGCI + "/changePassword"
        print "the URL to invoke for updating contact is: ",url
        print "EmailID",EmailID
        print "Password: ",Password
	try:
                requestBody = {
                                "updateContact": {
                                        "email":EmailID,
                                        "password":Password
                                }
                }
                headers = {'Content-type' : 'application/json','Accept' : 'text/plain'}
                res = req.post(url,data=json.dumps(requestBody),headers=headers)
                print "Afer sending the data to the server url is: ",res.url
                if res.status_code == 200:
                        requestAccepted=True
                        responseMessage = "UPDATE_SUCCESS"
                        print "responseMessage: ",responseMessage
                else:
                        requestAccepted=False
                        responseMessage = "UPDATE_FAILED"
                        print "responseMessage: ",responseMessage
                print "Response status", res.status_code
        except req.exceptions.RequestException as e:
                print e
                requestAccepted = False
                responseMsg = "ESB_SERVICE_ERROR"
        return responseMessage

def getContact(userN):
	print "userN: ",userN
	payload = {'email' : userN}
	customerResponseObj = {}
	url = dashboard_settings.ESB_ENDPOINT + "/getContact"
	try:
		r = req.get(url,params=payload);
		print (r.url)
		customerResponseObj = r.json()
	except  req.exceptions.RequestException as e:
                print e
                customerResponseObj = {"ErrorMsg" : "Error Updating Contact"}
        return customerResponseObj

#function for email change
def updateEmail(oldEmail,newemail,UserID,XToken):
        responseMessage = ""
        requestAccepted=False
        url = dashboard_settings.ESB_ENDPOINT + "/updateContact"
        print "the URL to invoke for updating contact is: ",url
        print "oldEmail: ",oldEmail
	print "newemail: ",newemail
	print "UserID: ",UserID
	print "XToken: ",XToken
        try:
                requestBody = {
                                "contactDetail": {
                                        "email":oldEmail,
                                        "email_new":newemail
                                }
                }
                headers = {'Content-type' : 'application/json','Accept' : 'text/plain', 'user-id' : UserID, 'X-Auth-Token' : XToken}
                res = req.put(url,data=json.dumps(requestBody),headers=headers)
                print "Afer sending the data to the server url is: ",res.url
                if res.status_code == 200:
                        requestAccepted=True
                        responseMessage = "UPDATE_SUCCESS"
                        print "responseMessage: ",responseMessage
                else:
                        requestAccepted=False
                        responseMessage = "UPDATE_FAILED"
                        print "responseMessage: ",responseMessage
                print "Response status", res.status_code
        except req.exceptions.RequestException as e:
                print e
                requestAccepted = False
                responseMsg = "ESB_SERVICE_ERROR"
        return responseMessage
