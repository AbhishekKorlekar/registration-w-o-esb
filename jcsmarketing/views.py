#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


from django.shortcuts import render
from openstack_dashboard.usage import quotas
from django.http import HttpResponseRedirect #for redirecting Abhishek
from django.shortcuts import redirect
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template
import logging
# from cities_light.models import City
# from cities_light.models import Region
from django.core.urlresolvers import reverse  # noqa
from django.core.urlresolvers import reverse_lazy  # noqa
from django import shortcuts
from django.template import RequestContext  # noqa
from django.views.generic import TemplateView
from jcsregistration.forms import UserSignupForm
from horizon import forms
from django.conf import settings
from django import http
import esb_Services as esb
import openstack_dashboard.api.neutron
import openstack_dashboard.api.keystone
import openstack_dashboard.settings as dashboard_settings
try:
    import simplejson as json
except ImportError:
    import json
#  Create your views here.


usages = quotas.JCSQuotaUsage()


def index(request):
#	print request.GET.get('name')
#	username = request.GET.get('name')
#	userid = request.GET.get('uuid')
#	XAuthToken = request.GET.get('xtoken')
#    if request.user.is_authenticated():
#        return render(request, 'jcsmarketing/index.html', {"name":username, "userid":userid, "XAuthToken":XAuthToken})
	return render(request, 'jcsmarketing/index.html')


def console(request):
#    if request.user.is_authenticated():
        if request.GET.get('JCSCompute'):
            ram = quotas.jcs_console_usages(request, usages, "compute")
        elif request.GET.get('JCSObjects'):
            ram = quotas.jcs_console_usages(request, usages, "objects")
        elif request.GET.get('JCSVPCs'):
            ram = quotas.jcs_console_usages(request, usages, "vpcs")
        else:
            ram = quotas.jcs_console_usages(request, usages)
        return render(request, 'jcsmarketing/console.html', {'usage': ram})


def compute(request):
    if request.user.is_authenticated():
        return render(request, 'jcsmarketing/compute.html')


def contactus(request):
    if request.user.is_authenticated():
        return render(request, 'jcsmarketing/contactus.html')


def products(request):
    if request.user.is_authenticated():
        return render(request, 'jcsmarketing/products.html')


def jcsdss(request):
    if request.user.is_authenticated():
        return render(request, 'jcsmarketing/jcsdss.html')


def jcsn(request):
    if request.user.is_authenticated():
        return render(request, 'jcsmarketing/jcsn.html')


def jcssbs(request):
    if request.user.is_authenticated():
        return render(request, 'jcsmarketing/jcssbs.html')


def pricing(request):
    if request.user.is_authenticated():
        return render(request, 'jcsmarketing/pricing.html')


def support(request):
    if request.user.is_authenticated():
        return render(request, 'jcsmarketing/support.html')

def profilepage(request):
	cookievalueXAuthToken = ""
	cookievalueuserid = ""
	cookievalueusername = ""
#	print "inside profilePage views: ",request.GET.get('name')
#	retrieving cookie value over here
	cookievalueXAuthToken = request.session["cookievalueXAuthToken"]  #obtaining cookie value XAuthToken
	cookievalueuserid = request.session["cookievalueuserid"]  #obtaining cookie value userid
	cookievalueusername = request.session["cookievalueusername"]  #obtaining cookie value username
	print "cookie value obtained XAuthToken: ",cookievalueXAuthToken
	print "cookie value obtained userid: ",cookievalueuserid
	print "cookie value obtained username: ",cookievalueusername
#	userid = request.GET.get('uuid')
#	XAuthToken = request.GET.get('xtoken')
	responseMsg = ""
	response_status = 200;
	responseObj = ""
#	res = esb.getContact(username)
	res = esb.getContact(cookievalueusername)
	if "getContactResponse" in res:
		contactInfo = res["getContactResponse"]
		name = contactInfo["last_name"]
		PhoneNo = contactInfo["phone_mobile"]
		AccName = contactInfo["account_name"]
#		provisionMsg = {"name": name,"phoneno":PhoneNo,"accountname":AccName,"userNaam":username, "userid":userid, "XAuthToken":XAuthToken}
		provisionMsg = {"name": name,"phoneno":PhoneNo,"accountname":AccName,"userNaam":cookievalueusername, "userid":cookievalueuserid, "XAuthToken":cookievalueXAuthToken}
	else:
		param="ERROR"
		provisionMsg = {"msg" : param}

	return render(request, 'jcsmarketing/profilepage.html', provisionMsg)

def updateName(request):
	responseMsg = ""
	name = ""
	phoneNo = ""
	res = None
	response_status = 200;
	responseObj = ""
	name = request.POST.get("name")
	phoneNo = request.POST.get("newphoneno")
	email = request.POST.get("hiddenurl")
	print "inside views for updateName"
	print "name from profilePage hidden field: ",name
	print "email: ",email
	print "phoneNo: ",phoneNo
	try:
		res = esb.updateName(email,name,phoneNo)
		if res == 'UPDATE_SUCCESS':
                        response_status = 200
#                       responseMsg = {'code':'200'}
                        res = 'ACCEPTED'
                       	print "UPDATE SUCCESSFUL"	
                else:
                        response_status = 400
#                       responseMsg = {'code':'400'}
                        print "UPDATE FAILED"
                responseMsg = {"code":res }
        except:
                print "\nException Occurred",sys.exc_info()[0]
                responseMsg = {'code':'UNKNOWN_ERROR'}
                response_status = 400
        finally:
                responseObj = HttpResponse(json.dumps(responseMsg),content_type="text/html")
                print "responseObj: ",responseObj
                responseObj.status_code = response_status
#	return HttpResponseRedirect("/profilePage?uuid="+userid+"&xtoken="+XAuthToken+"&name="+email)
	return HttpResponseRedirect("/profilePage/")

def updatePassword(request):
	responseMsg = ""
        password = ""
        email = request.POST.get("hiddenurl")
        res = None
        response_status = 200;
        responseObj = ""
        password = request.POST.get("newpassword")
        print "inside views for updatePassword"
        print "email: ",email
        print "password: ",password
	try:
                res = esb.updatePassword(email,password)
                if res == 'UPDATE_SUCCESS':
                        response_status = 200
#                       responseMsg = {'code':'200'}
                        res = 'ACCEPTED'
                        print "UPDATE SUCCESSFUL"
                else:
                        response_status = 400
#                       responseMsg = {'code':'400'}
                        print "UPDATE FAILED"
                responseMsg = { 'code' : res }
        except:
                print "\nException Occurred",sys.exc_info()[0]
                responseMsg = {'code':'UNKNOWN_ERROR'}
                response_status = 400
        finally:
                responseObj = HttpResponse(json.dumps(responseMsg),content_type="text/html")
                print "responseObj: ",responseObj
                responseObj.status_code = response_status
        return HttpResponseRedirect("/restyled/loginPage")

def updateEmail(request):
        responseMsg = ""
        newEmail = ""
        email = request.POST.get("hiddenurl")
        res = None
        response_status = 200;
        responseObj = ""
        newEmail = request.POST.get("newemailid")
	userid = request.POST.get("hiddenuserid")
	XAuthToken = request.POST.get("hiddenXAuthToken")
        print "inside views for updateEmail"
        print "oldemail: ",email
	print "newEmail: ",newEmail
	print "userid: ",userid
	print "XAuthToken: ",XAuthToken
        try:
                res = esb.updateEmail(email,newEmail,userid,XAuthToken)
                if res == 'UPDATE_SUCCESS':
                        response_status = 200
#                       responseMsg = {'code':'200'}
                        res = 'ACCEPTED'
                        print "UPDATE SUCCESSFUL"
                else:
                        response_status = 400
#                       responseMsg = {'code':'400'}
                        print "UPDATE FAILED"
                responseMsg = { 'code' : res }
        except:
                print "\nException Occurred",sys.exc_info()[0]
                responseMsg = {'code':'UNKNOWN_ERROR'}
                response_status = 400
        finally:
                responseObj = HttpResponse(json.dumps(responseMsg),content_type="text/html")
                print "responseObj: ",responseObj
                responseObj.status_code = response_status
        return HttpResponseRedirect("/restyled/loginPage")

