/*Javascript functions on "EDIT" button click - start*/
function hidename(){
        document.getElementById("name").style.border="1px solid #e5e9ec";
        document.getElementById("name").readOnly = false;
        document.getElementById("namesavebuttons").style.display = '';
        return false;
}

function hidephone(){
        document.getElementById("phonesave").style.display = '';
        return false;
}

function hideemail(){
        document.getElementById("emailsave").style.display = '';
        return false;
}

function hidepassword(){
        document.getElementById("passwordsave").style.display = '';
        document.getElementById("passwordsavebuttons").style.display = '';
        return false;
}
/*Javascript functions on "EDIT" button click - END*/

/* Javascript functions to hide the div tag on click of cancel - START*/
function cancel1(){
document.getElementById("name").style.border = 'none';
document.getElementById("name").readOnly = true;
document.getElementById("namesavebuttons").style.display = 'none';
x = document.getElementById("name")
var defaultVal = x.defaultValue;
x.value = defaultVal ;
return false;
}

function cancel2(){
document.getElementById("phonesave").style.display = 'none';
document.getElementById("phonesavebuttons").style.display = 'none';
document.getElementById("otpbuttons").style.display = 'none';
x = document.getElementById("newphoneno")
var defaultVal = x.defaultValue;
x.value = defaultVal ;
return false;
}

function cancel3(){
document.getElementById("emailsave").style.display = 'none';
document.getElementById("emailsavebuttons").style.display = 'none';
document.getElementById("emailotpbuttons").style.display = 'none';
x = document.getElementById("newemailid")
var defaultVal = x.defaultValue;
x.value = defaultVal ;
return false;
}

function cancel4(){
document.getElementById("passwordsave").style.display = 'none';
document.getElementById("passwordsavebuttons").style.display = 'none';
x = document.getElementById("newpassword")
var defaultVal = x.defaultValue;
x.value = defaultVal ;
return false;
}
/* Javascript functions to hide the div tag on click of cancel - END*/

/* Javascript function to verify the OTP sent - start*/
function verifyOTP(){
        var otp_entered = document.getElementById("otp").value;
        var otp_sent = document.getElementById("otpsent").value;
	var secret_key = "R4^&ad#$L";
	var decrypted = CryptoJS.AES.decrypt(otp_sent, secret_key);
	var finaldecryptedstring = decrypted.toString(CryptoJS.enc.Utf8);
        if(otp_entered == finaldecryptedstring){
                document.getElementById("otpbuttons").style.display = 'none';
                document.getElementById("savephone").disabled = false;
        }else{
                document.getElementById('otpmessage').innerHTML = 'Please enter the OTP, received on your mobile number';
        }
}
/* Javascript function to verify the OTP sent - end*/

/* Javascript function to verify the Email OTP sent - start*/
function verifyEmailOTP(){
        var otp_entered = document.getElementById("emailotp").value;
        var otp_sent = document.getElementById("emailotpsent").value;
	var secret_key = "R4^&ad#$L";
        var decrypted = CryptoJS.AES.decrypt(otp_sent, secret_key);
        var finaldecryptedstring = decrypted.toString(CryptoJS.enc.Utf8);
        if(otp_entered == finaldecryptedstring){
                document.getElementById("emailotpbuttons").style.display = 'none';
                document.getElementById("saveemail").disabled = false;
        }else{
                document.getElementById('emailotpmessage').innerHTML = 'Please enter the OTP, received on your Email Address';
        }
}
/* Javascript function to verify the Email OTP sent - end*/

/* Javascript function to perform name validations - start */
function validateName()
{
       var newnamevalue = document.getElementById("name").value ;
       name123 = $(document.getElementById("name"));
       namevalue =$(document.getElementById("name")).val();
       error2 = $(document.getElementById('namevalidation'));
       error2.show();
      if(validatecheck())
     {
              var filter = /^[a-zA-Z]+ {1}[a-zA-Z]+$/;
              if (filter.test(namevalue))
           {
                     document.getElementById("name").style.border="1px solid #e5e9ec";
                     return true;
           }
           else
           {
              document.getElementById("name").style.border="1px solid #F20E0E";
              error2.addClass("error");
              error2.text("*Please enter only Alphabets.");
              return false;
           }
     }
       else
           {
           return false;
           }
           function validatecheck()
           {
              if(namevalue == "")
                     {
                     document.getElementById("name").style.border="1px solid #F20E0E";
                     error2.addClass("error");
                     error2.text("*Name should be atleast 2 characters.");
                     return false ;
                     }
              else
                     {
                     document.getElementById("name").style.border="1px solid #e5e9ec";
                     return true;
                     }
       }
}
/* Javascript function to perform name validations - end */

/* Javascript function to perform phone validations - start */
function validatePhone()
{
       var newnamevalue = document.getElementById("newphoneno").value ;
       name123 = $(document.getElementById("newphoneno"));
       namevalue =$(document.getElementById("newphoneno")).val();
       error2 = $(document.getElementById('phonevalidation'));
       error2.show();
       if(validatecheck())
     {
              var filter = /^[0-9]*$/;
              if (filter.test(namevalue))
           {
                document.getElementById("newphoneno").style.border="1px solid #e5e9ec";
                document.getElementById("otpbuttons").style.display = '';
                document.getElementById("phonesavebuttons").style.display = '';
                error2.hide();
	 	$(document).ready(function(){
		var phonenumber = $('#newphoneno').val();
                var otp_code = randomNumber1();
		var otp_string_code = otp_code.toString();
		msg = "Hi, Thank you for registering with Reliance Jio. "+
                                  "Please use this pin "+otp_code+
                                  " to validate your phone. Thanks";
		var secret_key = "R4^&ad#$L";
		var encrypted = CryptoJS.AES.encrypt(otp_string_code, secret_key);
                $.ajax({
                url : 'http://enterprise.smsgupshup.com/GatewayAPI/rest',
                type : 'GET',
                data : {
                  'method' : 'SendMessage',
                  'send_to' : phonenumber,
                  'msg' : msg,
                  'msg_type' : 'TEXT',
                  'userid' : '2000150888',
                  'password' : 'ksWFuk',
                  'auth_scheme' : 'plain',
                  'v' : '1.1',
                  'format' : 'text'
                },
                success : function(data, textStatus, jqXHR) {
                        $("#otpsent").val(encrypted);
                },
                error : function(jqXHR, textStatus, errorThrown){
                        $("#otpsent").val(encrypted);
                },
                complete: function(xhr, textStatus){
                        console.log("complete block");
                }
          	});
		function randomNumber1(){
                        var num = 0;
                        num = Math.random();
                        num *= 10000000;
                        num = Math.round(num);
                        return num;
                };
           });
                     return true;
           }
           else
           {
              document.getElementById("newphoneno").style.border="1px solid #F20E0E";
              error2.addClass("error");
              error2.text("*Please enter only digits");
              return false;
           }
     }
       else
           {
           return false;
           }
	       function validatecheck()
	       {
              if(namevalue == "")
                     {
                     document.getElementById("newphoneno").style.border="1px solid #F20E0E";
                     error2.addClass("error");
                     error2.text("*PhoneNo is mandatory");
                     return false ;
                     }
              else
                     {
                     document.getElementById("newphoneno").style.border="1px solid #e5e9ec";
                     return true;
                     }
       }
}
/* Javascript function to perform phone validations - end*/

/* Javascript function to perform email validations - start*/
function validateEmail()
{
       var newnamevalue = document.getElementById("newemailid").value ;
       name123 = $(document.getElementById("newemailid"));
       namevalue =$(document.getElementById("newemailid")).val();
       error2 = $(document.getElementById('emailvalidation'));
       error2.show();
       if(validatecheck())
     {
              var filter = /^[a-zA-Z0-9]+(\.{1}[a-zA-Z0-9]+)?@{1}(([a-zA-Z]{1,67})|([a-zA-Z]+\.[a-zA-Z]{1,67}))\.(([a-zA-Z]{2,4})(\.[a-zA-Z]{2})?)$/;
              if (filter.test(namevalue))
           {
                     document.getElementById("newemailid").style.border="1px solid #e5e9ec";
                     document.getElementById("emailotpbuttons").style.display = '';
                     document.getElementById("emailsavebuttons").style.display = '';
                     error2.hide();
                $(document).ready(function(){
                var emailid = $('#newemailid').val();
                console.log("emailid: ",emailid);
                var otp_code = randomNumber();
		var otp_string_code = otp_code.toString();
		msg = "Hi, Thank you for registering with Reliance Jio. "+
                                  "Please use this pin "+otp_code+
                                  " to validate your email. Thanks";
		var secret_key = "R4^&ad#$L";
                var encrypted = CryptoJS.AES.encrypt(otp_string_code, secret_key);
                $.ajax({
                url : 'http://10.135.81.82:14280/il/sendEmail',
                type : 'POST',
                dataType : 'json',
                data : {
                        'msg' : msg,
                        'recName' : emailid,
                        'recAddress' : emailid
                },
                success : function(data, textStatus, jqXHR) {
                        $("#emailotpsent").val(encrypted);
                },
		error : function(jqXHR, textStatus, errorThrown){
                        $("#emailotpsent").val(encrypted);
                },
                complete: function(xhr, textStatus){
                        console.log("complete block");
                }
                });
                 function randomNumber(){
                        var num = 0;
                        num = Math.random();
                        num *= 10000000;
                        num = Math.round(num);
                        return num;
                };
                });
                     return true;
           }
           else
           {
              document.getElementById("newemailid").style.border="1px solid #F20E0E";
              error2.addClass("error");
              error2.text("*Please enter valid Email");
              return false;
           }
     }
       else
           {
           return false;
           }
	function validatecheck()
       	  {
              if(namevalue == "")
                     {
                     document.getElementById("newemailid").style.border="1px solid #F20E0E";
                     error2.addClass("error");
                     error2.text("*Email is mandatory");
                     return false ;
                     }
              else
                     {
                     document.getElementById("newemailid").style.border="1px solid #e5e9ec";
                     return true;
                     }
       }
}
/* Javascript function to perform email validations - end*/
