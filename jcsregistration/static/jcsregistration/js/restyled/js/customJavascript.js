/*$(document).ready(function(){   customJavascript for LoginPage
$('.otpButton').click(function() {
//send the AJAX call to the server
  $.ajax({
  //The URL to process the request
    url : 'https://webaroo-send-message-v1.p.mashape.com/sendMessage',
  //The type of request, also known as the "method" in HTML forms
  //Can be 'GET' or 'POST'
    type : 'GET',
  //Any post-data/get-data parameters
  //This is optional
    data : {
      'message' : 'Abhishek korlekar',
      'phone' : '7710064293'
    },
//    'headers': '{'X-Mashape-Key':'MROpZb3Se7mshCwITulCM68MbwGtp1suSwijsnUBX5rxGpBpnR'}'
    beforeSend: function(xhr){xhr.setRequestHeader('X-Mashape-Key', 'MROpZb3Se7mshCwITulCM68MbwGtp1suSwijsnUBX5rxGpBpnR');},
  //The response from the server
    success : function(data) {
    //You can use any jQuery/JavaScript here!!!
      if (data == "success") {
        alert('request sent!');
      }
    }
  });
});
});*/
/*function enableButton(){
	document.getElementById("signupbtn").disabled = false;
}
function showAlert(){
	alert("Hello");
}
function generateRandomNumber(){
	var randomnumber1=Math.floor(10000 + Math.random() * 90000)
   	var stringRefCode=randomnumber1.toString();
    	document.getElementById("randomvalue").value= stringRefCode
}
function submitotpform(){
	document.getElementById('otpform').submit();
}
function phoneNumberSync(){
//	var text1 = document.get
//	var n1 = document.getElementById('phone_number');
//	var n2 = document.getElementById('copyPhoneValue');
//	n2.value = n1.value;
}

$(document).ready(function(){
$("#cancelBtn").click(function(){
	$("#inputEmail").val("");
	$("#inputPassword").val("");
	});

//block 2 */
//jquery
var mainContainer = {
 mainFn : function(){
                //console.log("Hi this is main Function");
                mainContainer.formValidate();
                mainContainer.cancelSubmit();
        },ajaxLoader : {
                show : function(){
                        $("#ajaxLoader").modal({
                                'show' : true,
                                'keyboard' : false,
                                'backdrop' : 'static'
                        });
                },
                hide : function(){
                        $("#ajaxLoader").modal('hide');
                }
        },cancelSubmit : function(){
                $("#cancelBtn").click(function(){
                        $("#inputEmail").val("");
                        $("#inputPassword").val("");
                });
        },formValidate : function(){
		$("#demo_request_form").validate({
		rules: {
			inputEmail: {
                                  required: true,
                                  maxlength: 64,
				  email: true,
                                  pattern: /^[a-zA-Z0-9]+(\.{1}[a-zA-Z0-9]+)?@{1}(([a-zA-Z]{1,67})|([a-zA-Z]+\.[a-zA-Z]{1,67}))\.(([a-zA-Z]{2,4})(\.[a-zA-Z]{2})?)$/
                                }
		},highlight : function(element,errorClass){
                          $(element).css("border-color","red");
                },unhighlight : function(element){
                          $(element).css("border-color","");
                },errorPlacement : function(error,element){
			 if(element.attr("name") === "inputEmail"){
                        	 error.insertAfter(document.getElementById("inputEmail"));
				 error.css("color","#B22222");
				 error.css("font-style","italic");
				 error.css("font-size","70%");
                	 }
		},messages: {
				inputEmail: {
                                  pattern: 'Please enter a valid email address',
                                  required : 'This field is Mandatory'
                                }
		},submitHandler: function(form) {
        //            form.submit();
			$('#demo_request_form-result').empty();
                                mainContainer.ajaxLoader.show(); // show ajax loading image
                                $(form).ajaxSubmit({
                                  target: '#demo_request_form-result',
                                  success: function(responseText, statusText, xhr, $form){
                                    mainContainer.ajaxLoader.hide();
                                    var SUCCESS_msg = "<div class='alert alert-success'>"+
                                                        "Login Success</div>";
                                    $('#demo_request_form-result').empty();
                                    var res = $.parseJSON(responseText);
                                    if (res.code === 'ACCEPTED')
                                    {
					window.location.href = "/jcs-home/";
                                    }
                                  },
				   error: function(response, status, error){
                                            mainContainer.ajaxLoader.hide(); // hide ajax loading image
                                            var LOGIN_FAILED_error_msg = "<div class='errorMsg'>Invalid Username and Password</div>";
					    var SERVICE_DOWN_error_msg = "<div class='errorMsg'>Sorry! Service is temporarily unavailable</div>";
                                            var UNKNOWN_error_msg = "<div class='errorMsg'>Sorry! Request you to try again after some"+
                                                                        " time as we are experiencing some issues</div>";
					   if(response.status == 400){
                                                      $('#demo_request_form-result').empty();
                                                      var res = $.parseJSON(response.responseText);
                                                     	if (res.code === 'LOGIN_FAILED'){
                                                        $('#demo_request_form-result').append(LOGIN_FAILED_error_msg);
                                                         mainContainer.captcha.reset();
                                                      }
                                               }
					   if(response.status == 503){
                                                      $('#demo_request_form-result').empty();
                                                      var res = $.parseJSON(response.responseText);
                                                        if (res.code === 'SERVICE_DOWN'){
                                                        $('#demo_request_form-result').append(SERVICE_DOWN_error_msg);
                                                         mainContainer.captcha.reset();
                                                      }
                                               }
					}//error function close
				});//ajax form submit
			}//submit handler block 					
		});//$("#demo_request_form").validate
	}//form validate function
} //mainContainer object

$(document).ready(mainContainer.mainFn);
