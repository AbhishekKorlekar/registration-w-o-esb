
/*
	Main JS file which contains the code for user registeration flow.

*/

var mainContainer = {
	mainFn : function(){
		//console.log("Hi this is main Function");
		mainContainer.formValidate();
		mainContainer.captcha.init();
		mainContainer.cancelSubmit();
	},ajaxLoader : {
		show : function(){
			$("#ajaxLoader").modal({
				'show' : true,
				'keyboard' : false,
				'backdrop' : 'static'
			});
		},
		hide : function(){
			$("#ajaxLoader").modal('hide');
		}	
	},cancelSubmit : function(){
		$("#cancelBtn").click(function(){
			$("#firstname").val("");
			$("#lastname").val("");
			$("#organization").val("");
			$("#email_id").val("");
			$("#phone_number").val("");
			$("#defaultReal").val("");
			mainContainer.captcha.reset();
		});
	},captcha : {
		init : function(){
			$("#defaultReal").realperson({
                        	chars: $.realperson.alphanumeric,
               	        	length : 6
                	});
			var myHash = $("#defaultReal").realperson('getHash');
		//	var myHash = $("#defaultReal").val();
			console.log("Hash Value "+myHash);
                	$("#gen_hash_num").val(myHash);

		},reset : function(){
				$("#defaultReal").realperson('enable');
                                $("div.realperson-regen").click();
                                var myHash = $("#defaultReal").realperson('getHash');
                                $("#gen_hash_num").val(myHash);
		}	
	},formValidate : function(){		
		//console.log("Hi this is form validate function...");
		
                $("#demo_request_form").validate({
		      rules: {
				firstname: {
				  required: true,
				  minlength: 2,
				  maxlength: 64,
				  pattern: /^[a-zA-Z\-]+$/
				},
				lastname: {
				  required: true,
				  minlength: 2,
				  maxlength: 64,
				  pattern: /^[a-zA-Z\-]+$/
				},
				organization: {
				  required: true,
				  minlength: 2,
				  maxlength: 64,
				  pattern: /^(([A-Za-z0-9])[A-Za-z0-9 ]+)+$/
				},
				email_id: {
				  required: true,
				  maxlength: 64,
				  email: true
				},
				phone_number: {
				  required: true,
				  digits: true,
				  minlength: 10,
				  maxlength: 10
				},
				defaultReal: {
				  required: true
				}
		      },highlight : function(element,errorClass){
				$(element).css("border-color","red");
		      },unhighlight : function(element){
				$(element).css("border-color","");
		      },errorPlacement : function(error,element){
				if(element.attr("name") === "firstname"){
					error.insertAfter(document.getElementById("firstname"));
				}
				
				if(element.attr("name") === "lastname"){
					error.insertAfter(document.getElementById("lastname"));
				}
				
				if(element.attr("name") === "organization"){
					error.insertAfter(document.getElementById("organization"));
				}
				
				if(element.attr("name") === "email_id"){
					error.insertAfter(document.getElementById("email_id"));
				}
				
				if(element.attr("name") === "phone_number"){
					error.insertAfter(document.getElementById("phone_number"));
				}
				
				if(element.attr("name") === "defaultReal"){
					$("#captchaContainer").append(error);
				}
		      },messages: {
                                firstname: {
                                  required: 'This field cannot be blank',
                                  pattern: 'Please enter only alphabets without spaces and special characters and is atleast 2 characters long'
                                },
				lastname: {
                                  required: 'This field cannot be blank',
                                  pattern: 'Please enter only alphabets without spaces and special characters and is atleast 2 characters long'
                                },
                                organization: {
                                  required: 'This field is Mandatory',
                                  pattern: 'Organization Name should be atleast 2 characters'
                                },
                                email_id: {
                                  pattern: 'Please enter a valid email address',
				  required : 'This field is Mandatory'
                                },
                                phone_number: {
				  required : 'Please enter a 10 digit phone number',
                                  minlength: 'Please enter 10 digits.',
                                  maxlength: 'Please enter 10 digits.'
                                },
                                defaultReal: {
                                  minlength: 'Invalid captcha entered',
                                  maxlength: 'Invalid captcha entered',
				  required : 'Invalid captcha entered'
                                }
                       },submitHandler: function(form) {
                                
				$('#demo_request_form-result').empty();
				mainContainer.ajaxLoader.show(); // show ajax loading image 
    				$(form).ajaxSubmit({
                                  target: '#demo_request_form-result',
                                  success: function(responseText, statusText, xhr, $form){
				    mainContainer.ajaxLoader.hide();
				    var SUCCESS_msg = "<div class='alert alert-success'>"+
							"Your request has been recorded. You will soon receive an email from us"+
							" to complete your registration.</div>";
                                    $('#demo_request_form-result').empty();
                                    var res = $.parseJSON(responseText);
                                    if (res.code === 'ACCEPTED')
                                    {
					window.location.href = "registerSuccess";
                                    }
                                  },
                                  error: function(response, status, error){
					    mainContainer.ajaxLoader.hide(); // hide ajax loading image
					    $("#defaultReal").val("");//removing old captcha input value
					    var LEAD_ALREADY_EXISTS_error_msg = "<div class='errorMsg'>"+
									"You are already registered with us. Kindly allow us some more time in approving"+
									" your account. If you are here for the first time, Please sign up with a different"+
									" email address. Thank you."+
								      "</div>";
	
					    var USER_ALREADY_EXISTS_error_msg = "<div class='errorMsg'>"+
									"You are already registered with us. Please login <a href='{{login_url}}'>here</a>"+
									" to proceed. </div>"; 		
						
					    var TENANT_ALREADY_EXISTS_error_msg = "<div class='errorMsg'>"+
									"This organisation name is already registered with us. "+
									"Please use different organisation name. Thank you.</div>";
					
					    var CAPTCHA_error_msg = "<div class='errorMsg'>Captcha Image verification failed.</div>";
					    var UNKNOWN_error_msg = "<div class='errorMsg'>Sorry! Request you to try again after some"+
									" time as we are experiencing some issues</div>"; 									 var CRM_ERROR_error_msg = "<div class='errorMsg'>Sorry! We Request you to try again after some"+
                                                                        " time as we are experiencing some issues in CRM</div>";	
					       if(response.status == 400){

						      $('#demo_request_form-result').empty();
						      var res = $.parseJSON(response.responseText);
						      if (res.code === 'LEAD_ALREADY_EXISTS'){
							$('#demo_request_form-result').append(LEAD_ALREADY_EXISTS_error_msg);
							 mainContainer.captcha.reset();
						      }
						      if (res.code === 'USER_ALREADY_EXISTS'){
							$('#demo_request_form-result').append(USER_ALREADY_EXISTS_error_msg);
							$('#demo_request_form').fadeOut(500);
							 mainContainer.captcha.reset();
						      } 
						      if (res.code === 'TENANT_ALREADY_EXISTS'){
							$('#demo_request_form-result').append(TENANT_ALREADY_EXISTS_error_msg);
							 mainContainer.captcha.reset();
						      }
						      if (res.code === 'CAPTCHA'){
							$('#demo_request_form-result').append(CAPTCHA_error_msg);
							 mainContainer.captcha.reset();
						      }
						      if (res.code === 'UNKNOWN_ERROR' || res.code === 'ESB_SERVICE_ERROR'){
							$('#demo_request_form-result').append(UNKNOWN_error_msg);
							 mainContainer.captcha.reset();
						      }
						      if (res.code === 'CRM_ERROR'){
                                                        $('#demo_request_form-result').append(CRM_ERROR_error_msg);
                                                         mainContainer.captcha.reset();
                                                      }
					       }
                                         }// error method
                                });//Ajax form submit
                       } // end of submitHandler method
                });// end of validate block
	} // formValidate method
}// mainContainer object

$(document).ready(mainContainer.mainFn);
