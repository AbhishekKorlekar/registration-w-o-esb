import requests as req
import json
from keystoneclient.auth.identity import v3 as identityV3
from keystoneclient import session
from neutronclient.neutron import client as neutronClient
import sys
import openstack_dashboard.settings as dashboard_settings
#import urllib3

# Get Customer Info API
def getCustomerInfo(customerId):
	payload = {'custid' : customerId}
	customerResponseObj = {}
	url = dashboard_settings.ESB_ENDPOINTGCI + "/getCustInfo"
	try:
		r = req.get(url,params=payload);
		print (r.url)
		#print "the json response ",r.json()

		# the output will be {'customerinfo ' : {} }
		customerResponseObj = r.json()
	except  req.exceptions.RequestException as e:
		print e
		customerResponseObj = {"ErrorMsg" : "Error Connecting getCustInfo"}
	return customerResponseObj


# OTP Generation API
def generateOtp(emailId,mobileNo):
	url = dashboard_settings.ESB_ENDPOINT + "/sendOtp"
	OTP_GENERATED = False
	try:
		requestBody = {
			"user" : {
				"email" : emailId,
				"mobile" : mobileNo
			}
		}
		
		headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
		
		res = req.post(url,data=json.dumps(requestBody),headers=headers)
		print "URL : ",res.url
		print "RESPONSE-CODE ",res.status_code

		if res.status_code == 200:
			OTP_GENERATED = True
	
	except  req.exceptions.RequestException as e:
		print e
		OTP_GENERATED = False

	return OTP_GENERATED


# Provision User
def provisionUser(customer_name,customer_emailId,otp,customer_phoneNo):
	userProvisionStatus = False	
	url = dashboard_settings.ESB_ENDPOINTGCI + "/provisionUser"
	responseObj = {}	
	try:
		print "Calling ESB's provisionUser"
		requestBody = {
			"provisionaccount" : {
				"name" : customer_name,
				"email" : customer_emailId,
				#"password" : customer_password,
				"otp" : otp,
				"phonenumber" : customer_phoneNo 
			}
		}
		
		headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
		res = req.post(url,data=json.dumps(requestBody),headers=headers)
		
		print "URL : ",res.url
		print "RESPONSE-CODE ",res.status_code			
		responseObj = res.json()
		print "STATUS AFTER SERVICE 'provisionUser' ",responseObj["provisionaccountResponse"]["code"]	
	except  req.exceptions.RequestException as e:
		print e
		userProvisionStatus = True
		responseObj = {
			"provisionaccountResponse" : {
				"msg" : "ESB INVOCATION ERROR",
				"code" : "400"
			}
		}

	return responseObj


# Create Lead API
def createLead(customerName,subject,email,phoneNumber,message,organisation):
	leadExists = False
	tenantExists = False
	contactExists = False
	errorResponse = ""
	requestAccepted=False
	responseMsg = ""
	url = dashboard_settings.ESB_ENDPOINT + "/createLead"
	try:
		print "Invoking Create Lead"	
		requestBody = { 
				"createlead" : {
					"last_name" : customerName,
					"subject" : subject,
					"email1" : email,
					"lead_source" : "Website",
					"phone_mobile" : phoneNumber,
					"message" : message,				
					"org_name" : organisation
				}
		}
	
		headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
	
	
		res = req.post(url,data=json.dumps(requestBody),headers=headers)
	
		print "POST URL ",res.url
		
		if res.status_code != 201:
			print "RESPONSE CONTENT ",res.json()
			responseJSON = res.json()
		
		if res.status_code == 201: 
			#print ">>>>> ",responseJSON["blah"]["e"]
			requestAccepted = True
			responseMsg = "LEAD_CREATION_SUCCESS"
		else:
			errorResponse = responseJSON["ErrorResponse"]["msg"]
			print "Error Response : ",errorResponse
			requestAccepted = False

		if ((requestAccepted is False) and ("Lead" in errorResponse)):
			leadExists = True
			responseMsg = "LEAD_ALREADY_EXISTS"

		elif ((requestAccepted is False) and  ("Contact" in errorResponse)):
			contactExists = True
			responseMsg = "USER_ALREADY_EXISTS"

		elif ((requestAccepted is False) and  ("Tenant" in errorResponse)):
			tenantExists = True	
			responseMsg = "TENANT_ALREADY_EXISTS"
		
		elif (requestAccepted is False):
			responseMsg = "UNKNOWN_ERROR"
		else:
			pass
	
		print "RESPONSE CODE ",res.status_code
		print "LEAD EXISTS ",leadExists
		print "CONTACT Exists ",contactExists
		print "TENANT exists ",tenantExists

	except req.exceptions.RequestException as e:
		print e
		requestAccepted = False
		responseMsg = "ESB_SERVICE_ERROR"	
		
	return responseMsg

# Generate authentication token required openstack api calls
def getToken():
	authObj = ""
	sessionObj = ""
	username = dashboard_settings.ADMIN_USER
	password = dashboard_settings.ADMIN_PASSWORD
	auth_url= dashboard_settings.KEYSTONE_ENDPOINT
	token = ""
	try:
		#print "IN GET-TOKEN"	
	 			
		authObj = identityV3.Password(auth_url=auth_url,
					   username=username,
					   password=password,
 					   user_domain_id='default')
		
		sessionObj = session.Session(auth=authObj)
		token = authObj.get_token(sessionObj) 
		
	except:
		print "Exception OCCURED",sys.exc_info()[0]

	return token

# create Networing Module
def create_network(tenant_id,authToken):
	neutron_obj = ""
	neutron_endpoint = dashboard_settings.NEUTRON_ENDPOINT 
	gen_token = ""
	network_uuid = ""
	subnet_uuid = ""
	isCreated = False
	subnet = {}
	network = {}
	try:
		print "creating network for Tenant_ID ",tenant_id
		print "authToken obtained from ESB: ",authToken

#		gen_token = getToken()
		gen_token = authToken
		print "X-AUTH-TOKEN ",gen_token

		neutron_obj = neutronClient.Client('2.0', endpoint_url=neutron_endpoint, token=gen_token)
		neutron_obj.format = "json"
		network = {
			'network' : {
				'name' : 'default-network',
				'admin_state_up' : True,
				'tenant_id' : tenant_id
			}
		}
			
		obj = neutron_obj.create_network(network)
		if ((obj is not None) and ('network' in obj)):
                	network_uuid = obj['network']['id']
			print "New network created with NETWORK_ID ",network_uuid

			subnet = {
				'subnet' : {
					'name' : 'default-subnet',
					'network_id' : network_uuid,
					'tenant_id' : tenant_id,
					'ip_version' : '4',
					'cidr' : '172.16.0.0/20'
				}
			}

			obj = neutron_obj.create_subnet(subnet)
			
			if ((obj is not None) and ('subnet' in obj)):
				subnet_uuid = obj['subnet']['id']
				isCreated = True
				print "New Subnet created with SUBNET_ID ",subnet_uuid
			else:
				isCreated = False
		else:
			isCreated = False
		
		# Below code for testing perpose
		#neutron_obj.delete_subnet(subnet_id)
		#neutron_obj.delete_network(network_id)

	except:
		print "\nException OCCURED",sys.exc_info()[0]
		
		if ((subnet_uuid != "") or (subnet_uuid is not None)):
			neutron_obj.delete_subnet(subnet_uuid)
	 	
		if ((network_uuid != "") or (network_uuid is not None)):
			neutron_obj.delete_network(network_uuid)
			

		isCreated = False

	return isCreated

# hides all middle charaters of email Id leaving only
# first and last charater as it is.
def email_encrypt(emailId):
	myList = None
	myEmail = None
	email_char = None
	total_char = 0
	count = 0
	new_email = []
	try:
		#print "EMAIL IS ",emailId
		myList = emailId.split('@')	
		myEmail = myList[0]
		email_char = list(myEmail)
		total_char = len(email_char)
		for e in email_char:
			if count == 0:
				new_email.append(e)
				count = count + 1
			elif (total_char == 2) and (count == 1):
				new_email.append("*")
				count = count + 1
			elif total_char == (count + 1):
				new_email.append(e)
				count = count + 1
			else:
				new_email.append("*")
				count = count + 1
		
		myEmail = "".join(new_email) + "@" + myList[1]
		#print myEmail	
	except:
	  print "EXCEPTION IN email encryption ",sys.exc_info()[0]

	return myEmail	

#print "IS-NETWORK-CREATED ",create_network('0df519be853c4064b6dcb2e95e60b1f4')

#print "TOKEN ",getToken()

#print "Customer Info output ",getCustomerInfo(100)

#print "CREATE-LEAD ",createLead("Jimmy Page","This is dummy mail","Jimmy@ril.com","9867934525","From Jio Website")

#print "GENERATE OTP ",generateOtp("ninad.kulkarni@ril.com","9867934525")

#print "PROVISION USER ",provisionUser("Vishesh Parekh","vishesh.parekh@ril.com","vishesh234@1","1234","9867934525")

# UserLogin API
def userLogin(userNaam,userPass,requestSystem):
	responseMessage = {}
	userid = ""
	XAuthToken = ""
	requestAccepted=False
	if requestSystem == "KEYSTONE":
		url = dashboard_settings.ESB_ENDPOINT + "/userLogin"
	else:
		url = dashboard_settings.ESB_ENDPOINT + "/crmLogin"
	print "invoking ESB API call userLogin inside esbServices.py"
	print "url: ",url
	print "userNaam: ",userNaam
	print "userPass: ",userPass
	try:
		requestBody = {
				"auth": {
					"name":userNaam,
					"password":userPass
					}
				}
	        headers = {'Content-type' : 'application/json','Accept' : 'text/plain'}
       		res = req.post(url,data=json.dumps(requestBody),headers=headers)
	        print "Afer sending the data to the server url is: ",res.url
		
        	if res.status_code == 200:
			print "inside if condition for status 200"
			requestAccepted=True
			code = "LOGIN_SUCCESS"
			if requestSystem == "KEYSTONE":
				userid = res.headers['user-id']
				XAuthToken = res.headers['X-Auth-Token']
				print "the user id obtained from response header is: ",userid
	               		print "the X-Auth-Token obtained after response is: ",XAuthToken
			else:
				print "login happend through crm so no user-id and authtoken"
	               	responseMessage = {"code":code, "user-id":userid,"X-Auth-Token":XAuthToken}
#			responseMessage = {"code":code}
			print "responseMessage: ",responseMessage
		elif res.status_code == 401:
			print "inside if condition for status 401"
			requestAccepted=False
                      	code = "LOGIN_FAILED"
	                responseMessage = {"code":code}
	                print "responseMessage: ",responseMessage
		else:
			print "inside else condition"
			requestAccepted=False
			code = "SERVICE_DOWN"
			responseMessage = {"code":code}
			print "responseMessage: ",responseMessage
	        print "Response status", res.status_code
	except req.exceptions.RequestException as e:
                print e
                requestAccepted = False
                responseMsg = "ESB_SERVICE_ERROR"
   	return responseMessage

def sendOTP(msg,phoneNumber):
	responseMsg = ""
	print "Inside sendOTP function in esbServices.py"
	print "the random number that would be sent to the user: ",msg
	print "phoneNumber: ",phoneNumber
	responseMsg = ""
	payload = {'message':msg,'phone':phoneNumber}
	headerss = {'X-Mashape-Key':'MROpZb3Se7mshCwITulCM68MbwGtp1suSwijsnUBX5rxGpBpnR'}
	url = "https://webaroo-send-message-v1.p.mashape.com/sendMessage"
	try:
		r = req.get(url,params=payload,headers=headerss);
		print "RESPONSE STATUS: ",r.status_code
		responseMsg = r.status_code
		print "URL: ",r.url
		print "PARAMS: ",r.params
		print "HEADERS: ",r.headers
#	except  req.exceptions.RequestException as e:
	except AttributeError:
#		print e
		print "exception occured"
	return responseMsg
