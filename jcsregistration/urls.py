from django.conf.urls import url

from django.views.generic import TemplateView
from . import views

urlpatterns = [
       url(r'^jcsregistration/$', views.index, name='index'),
       url(r'^jcsregistration/signup/$',views.UserSignupView.as_view(),name='signup'),
       url(r'^userValidate',views.userValidate),
       url(r'^registerUser',views.registerUser),
       url(r'^loadRegister',views.gotoUserRegister),
       url(r'^Login',views.LoginUser),
       url(r'^sendOTP',views.sendotp),
       url(r'^restyled/loadRegister',TemplateView.as_view(template_name="jcsregistration/restyled/registerNewUser.html")),
       url(r'^restyled/sendemail',TemplateView.as_view(template_name="jcsregistration/restyled/sendEmail.html")),
       url(r'^restyled/registerSuccess',TemplateView.as_view(template_name="jcsregistration/restyled/registerSuccess.html")),
       url(r'^restyled/loginPage',views.delloginPageCookie),
       url(r'^restyled/userValidate',views.validationConfirm),
       url(r'^sendEmail',views.sendEmail),
      ]
