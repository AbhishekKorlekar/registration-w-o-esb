import requests as req
import json
import random
import string
from keystoneclient.auth.identity import v3 as identityV3
from keystoneclient import session
from neutronclient.neutron import client as neutronClient
import sys
import openstack_dashboard.settings as dashboard_settings
import urllib
from collections import OrderedDict
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

#crm funcion to Login
def registerUser(fullname,orgName,emailadd,phonenumber):
	url = dashboard_settings.CRM_ENDPOINT
	print "CRM URl: ",url
	loginResponse = Login("CRM")
	print "loginResponse: ",loginResponse
	if "id" in loginResponse:
		sessionID = loginResponse["id"]
		print "sessionID: ",sessionID
		tenantname = AccountRandomNumber()
		createaccount_name_value_list = [{"name":"name","value":tenantname},{"name":"account_type","value":"Tenant"},{"name":"quota_c","value":"DEFAULT"}]
		AccountRes = OrderedDictionary(sessionID, "Accounts", createaccount_name_value_list, "set_entry", "", "")
		if "id" in AccountRes:
			accountid = AccountRes["id"]
			createlead_name_value_list = [{"name":"last_name","value":fullname},{"name":"email1","value":emailadd},{"name":"phone_mobile","value":phonenumber},{"name":"account_id", "value":accountid},{"name":"organisation_name_c","value":orgName}]
			print "accountid: ",accountid
			LeadRes = OrderedDictionary(sessionID, "Leads", createlead_name_value_list, "set_entry", "", "")
			if "id" in LeadRes:
				responseMsg = "ACCEPTED"
			else:
				createLeadStatusCode = LeadRes["number"]
				print "creatLeadStatusCode: ",createLeadStatusCode
				if createLeadStatusCode == 1014:
					responseMsg = "USER_ALREADY_EXISTS"
				elif createLeadStatusCode == 1016:
					responseMsg = "LEAD_ALREADY_EXISTS"
				else:
					responseMsg = "TENANT_ALREADY_EXISTS"
		else:
			createAccountStatusCode = AccountRes["number"]
			print "createAccountStatusCode: ",createAccountStatusCode
			if createAccountStatusCode == 1015:
				responseMsg = "TENANT_ALREADY_EXISTS"
			else:
				print "account id not obtained"
				responseMsg = "CRM_ERROR"
				print responseMsg
	else:
		print "not found sessionID, crm might be down"
		responseMsg = "CRM_ERROR"
		print responseMsg
	return responseMsg

def OrderedDictionary(sessionid, modName, name_value_list,action, customerId, emailaddress):
	requestBody = OrderedDict()
	url = dashboard_settings.CRM_ENDPOINT
	headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
	if action == "set_entry":
		print "set_entry"
		payload = {'method' : 'set_entry', 'input_type' : 'JSON', 'response_type' : 'JSON'}
		requestBody["session"] = sessionid
		requestBody["module_name"] = modName
		requestBody["name_value_list"] = name_value_list
	elif action == "get_entry":
		print "get_entry"
		payload = {'method' : 'get_entry', 'input_type' : 'JSON', 'response_type' : 'JSON'}
                requestBody["session"] = sessionid
                requestBody["module_name"] = "Leads"
                requestBody["id"] = customerId
                requestBody["select_fields"] = name_value_list
	elif action == "search_by_module":
		print "search_by_module"
		payload = {'method' : 'search_by_module', 'input_type' : 'JSON', 'response_type' : 'JSON'}
		requestBody["session"] = sessionid
		requestBody["search_string"] = emailaddress
		requestBody["module_name"] = modName
		requestBody["offset"] = "0"
		requestBody["max_results"] = "10"
		requestBody["id"] = ""
		requestBody["select_fields"] = name_value_list

	res = req.post(url,data=json.dumps(requestBody),headers=headers,params=payload);
	resResponseObj = res.json()
	print "resResponseObj: ",resResponseObj
	return resResponseObj


def AccountRandomNumber():
	randomNumber = random.randint(100000000000,999999999999)
	print "randomNumber: ",randomNumber
	return randomNumber

def RandomPassword():
	randomPassword = ''.join([random.choice(string.ascii_letters + string.digits + string.punctuation) for n in xrange(8)])
	print "randomPassword: ",randomPassword
	return randomPassword

def sendEmail(lastname,emailid,randompass):
	print "inside sendEmail function in crm_Services.py"
	print "last_name: ",lastname
	print "emailid: ",emailid
	print "randompass: ",randompass
	me = "abhishek.korlekar@ril.com"
	you = emailid
# Create message container - the correct MIME type is multipart/alternative.
	msg = MIMEMultipart('alternative')
	msg['Subject'] = "Jio Cloud Service Access"
	msg['From'] = me
	msg['To'] = you
	# Create the body of the message (a plain-text and an HTML version).
	text = "Dear "+lastname+"\nYour temporary password is "+randompass+"\nPlease change this password immediately after your first login here: http://demo.jiocloudservices.com/\nEnsure the password is highly secure with a combination of small, Capital, numerical and special characters\n\nThanks,\nJio Cloud Services\n\nDisclaimer: Please do not share your password with any one."
	#       html = """\
	#       <html>
	#         <head></head>
	#         <body>
	#           <p>Hi!<br>
	#              How are you?<br>
	#              Here is the <a href="http://www.python.org">link</a> you wanted.
	#           </p>
	#         </body>
	#       </html>
	#       """
	# Record the MIME types of both parts - text/plain and text/html.
	part1 = MIMEText(text, 'plain')
	#       part2 = MIMEText(html, 'html')
	# Attach parts into message container.
	# According to RFC 2046, the last part of a multipart message, in this case
	# the HTML message, is best and preferred.
	msg.attach(part1)
	#       msg.attach(part2)
	# Send the message via local SMTP server.
	s = smtplib.SMTP('10.137.2.24', 25)
	# sendmail function takes 3 arguments: sender's address, recipient's address
	# and message to send - here it is sent as one string.
	s.sendmail(me, you, msg.as_string())
	s.quit()
	emailSent = "yes"
	print "success"
	return emailSent



def Login(loginSystem):
	headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
	if loginSystem == "KEYSTONE":
		url = dashboard_settings.KEYSTONE_ENDPOINT + "/v3/auth/tokens"		
		print "url: ",url
		requestBody =  {"auth": 
				 {"identity": {   
				 "methods": ["password"], 
				 "password": {   
				 "user": {    
				 "domain": {     
				 "name": "default"}, 
				 "name": "rahul",    
				 "password": "rahul"
				   }     
				  }      
				 }      
				}      
		      	 	} 
		keystoneloginresult = req.post(url,data=json.dumps(requestBody),headers=headers);
		keystoneLoginStatusCode = keystoneloginresult.status_code
		print "keystoneLoginStatusCode: ",keystoneLoginStatusCode
		if keystoneLoginStatusCode == 201:
			XSubjectToken = keystoneloginresult.headers['X-Subject-Token']
			print "X-Subject-Token: ",XSubjectToken
			keystoneResult = {'code' : keystoneLoginStatusCode, 'XSubjectToken': XSubjectToken}
		else:
			keystoneResult = {'code' : keystoneLoginStatusCode}
		return keystoneResult
	else:
		url = dashboard_settings.CRM_ENDPOINT
		payload = {'method' : 'login', 'input_type' : 'JSON', 'response_type' : 'JSON'}
		try:
			requestBody = {
					"user_auth":
					{
					"user_name":"admin",
					"password":"0192023a7bbd73250516f069df18b500",
					"application_name":"ESB",
					"name_value_list":[]
					}
				}
			loginresult = req.post(url,data=json.dumps(requestBody),headers=headers,params=payload);
			status_code_login = loginresult.status_code
			print "status_code_login: ",status_code_login
			loginResponseObj = loginresult.json()
			print "loginResponseObj: ",loginResponseObj
			loginResponseStatusCode = loginresult.status_code
		except req.exceptions.RequestException as e:
			print e
	return loginResponseObj	

def getCustomerInfo(customerId):
	headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
	url = dashboard_settings.CRM_ENDPOINT
	print "CRM URl: ",url
	loginResponse = Login("CRM")
	print "loginResponse: ",loginResponse
	if "id" in loginResponse:
		print "inside login rsponse id"
		sessionID = loginResponse["id"]
		select_fields_getEntry = ["last_name","email1","phone_mobile","account_name"]
		LeadResult = OrderedDictionary(sessionID, "Leads", select_fields_getEntry, "get_entry", customerId, "")
		if "entry_list" in LeadResult:
			entry_list_array = LeadResult["entry_list"]
			entry_list_inner_elements = entry_list_array[0]
			last_name = entry_list_inner_elements["name_value_list"]["last_name"]["value"]
			emailAddress = entry_list_inner_elements["name_value_list"]["email1"]["value"]
			phoneNo = entry_list_inner_elements["name_value_list"]["phone_mobile"]["value"]
			accountName = entry_list_inner_elements["name_value_list"]["account_name"]["value"]
			keystoneloginresult = Login("KEYSTONE")
			authToken = keystoneloginresult['XSubjectToken']
			keystoneloginstatuscode = keystoneloginresult['code']	
			print "authToken: ",authToken
			if keystoneloginstatuscode == 201:
				projectCreationLists = {"description":accountName,"domain_id":"default","enabled":"true","name":accountName}
				requestBody2 = {
					"project":{
					"description":"hello",
					"domain_id":"default",
					"name":accountName
					}}
				print "requestBody2: ",requestBody2
				keystoneHeaders = {'Content-type': 'application/json', 'Accept': 'application/json', 'X-Auth-Token': authToken}
				print "keystoneHeaders: ",keystoneHeaders
				projectURL = dashboard_settings.KEYSTONE_ENDPOINT + "/v3/projects"
				print "ProjectURL: ",projectURL
				keystoneProjectResult = req.post(projectURL,data=json.dumps(requestBody2),headers=keystoneHeaders);
				keystoneProjectResultResponseObj = keystoneProjectResult.json()
				keystoneprojectstatuscode = keystoneProjectResult.status_code
				if keystoneprojectstatuscode == 201:
					keystoneProjectid = keystoneProjectResultResponseObj["project"]["id"]
					print "keystoneProjectid: ",keystoneProjectid
					modName = ['Contacts']
					selFields = ['id']
					ContactIDResult = OrderedDictionary(sessionID, modName, selFields, "search_by_module", "",emailAddress)
					if "entry_list" in ContactIDResult:
						contactID = ContactIDResult["entry_list"][0]["records"][0]["id"]["value"]
						print "contactID: ",contactID
						randomPass = RandomPassword()
						print "randomPass: ",randomPass
						createContactPass_name_value_list = [{"name":"id","value":contactID},{"name":"password_c","value":randomPass}]
						ContactPassRes = OrderedDictionary(sessionID, "Contacts", createContactPass_name_value_list, "set_entry", "", "")
						if "id" in ContactPassRes:
							keystoneUserURL = dashboard_settings.KEYSTONE_ENDPOINT + "/v3/users"
							requestBody5 = {"user":{
									"default_project_id":keystoneProjectid,
									"name":emailAddress,
									"email":emailAddress,
									"password":randomPass
								}}
							keystoneUserResult = req.post(keystoneUserURL,data=json.dumps(requestBody5),headers=keystoneHeaders);
							keystoneUserResultResponseObj = keystoneUserResult.json()
							keystoneUserID = keystoneUserResultResponseObj["user"]["id"]
							print "keystoneUserID: ",keystoneUserID
							keystoneUserResultStatusCode = keystoneUserResult.status_code
							print "keystoneUserResultStatusCode: ",keystoneUserResultStatusCode
							if keystoneUserResultStatusCode == 201:
								print "check1"
								payload4 = {'name': 'admin'}
								print "payload4: ",payload4
								getRoleURL = dashboard_settings.KEYSTONE_ENDPOINT + "/v3/roles"
								print "getRoleURL: ",getRoleURL
								keystoneGetRoleIDResult = req.get(getRoleURL,params=payload4,headers=keystoneHeaders);
								print "keystoneGetRoleIDResult: ",keystoneGetRoleIDResult
								keystoneGetRoleIDResultResponseObj = keystoneGetRoleIDResult.json()
								print "keystoneGetRoleIDResultResponseObj: ",keystoneGetRoleIDResultResponseObj
								adminRoleID = keystoneGetRoleIDResultResponseObj["roles"][0]["id"]
								print "adminRoleID: ",adminRoleID
								keystoneGetRoleIDResultStatusCode = keystoneGetRoleIDResult.status_code
								if keystoneGetRoleIDResultStatusCode == 200:
									updateRoleURL = dashboard_settings.KEYSTONE_ENDPOINT + "/v3/projects/" + keystoneProjectid + "/users/" + keystoneUserID + "/roles/" + adminRoleID
									print "updateRoleURL: ",updateRoleURL
									updateRoleResult = req.put(updateRoleURL,headers=keystoneHeaders)
									updateRoleResultStatusCode = updateRoleResult.status_code
									print "updateRoleResultStatusCode: ",updateRoleResultStatusCode
									if updateRoleResultStatusCode == 204:
										neutron_endpoint = dashboard_settings.NEUTRON_ENDPOINT
										neutron_obj = neutronClient.Client('2.0', endpoint_url=neutron_endpoint, token=authToken)
										neutron_obj.format = "json"
										network = {
												'network' : {
												'name' : 'default-network',
												'admin_state_up' : True,
												'tenant_id' : keystoneProjectid 
												}
											 }
										obj = neutron_obj.create_network(network)
										if ((obj is not None) and ('network' in obj)):
											print "Network created successfully"
											network_uuid = obj['network']['id']
											print "New network created with NETWORK_ID ",network_uuid
											subnet = {
												'subnet' : {
												'name' : 'default-subnet',
												'network_id' : network_uuid,
												'tenant_id' : keystoneProjectid,
												'ip_version' : '4',
												'cidr' : '172.16.0.0/20'
												}
											}
											obj = neutron_obj.create_subnet(subnet)
											if ((obj is not None) and ('subnet' in obj)):
												print "subnets created successfully"
												subnet_uuid = obj['subnet']['id']
												print "New Subnet created with SUBNET_ID ",subnet_uuid
												encryptedEmail = email_encrypt(emailAddress)
												emailres = sendEmail(last_name,emailAddress,randomPass)
												if emailres == "yes":
													respObj = {'code':'SUCCESSFUL','mail' : encryptedEmail}
												else:
													respObj = {'code' : 'ERROR'}
													"temporary password not sent"
											else:
												respObj = {'code' : 'ERROR'}
												print "subnet creation failed"
										else:
											respObj = {'code' : 'ERROR'}
											print "create network failed"
									else:
										respObj = {'code' : 'ERROR'}
										print "role id not updated"
								else:
									respObj = {'code' : 'ERROR'}
									"Keystone Role ID not obtained"
							else:
								respObj = {'code' : 'ERROR'}
								print "Keystone User did not created"
						else:
							respObj = {'code' : 'ERROR'}
							"Contact Pass did not get updated"
					else:
						respObj = {'code' : 'ERROR'}
						print "Contact id  not obtained from CRM"
				else:
					respObj = {'code' : 'ERROR'}
					print "project did not get created"
			else:	
				respObj = {'code' : 'ERROR'}
				print "login failed"
		else:
			respObj = {'code' : 'ERROR'}
			print "CRM might be down"	
	else:
		respObj = {'code' : 'ERROR'}
		print "CRM might be down"
	#except  req.exceptions.RequestException as e:
	#	print e
	#	respObj = {'code' : 'ERROR'}
	return respObj


def email_encrypt(emailId):
	myList = None
	myEmail = None
	email_char = None
	total_char = 0
	count = 0
	new_email = []
	try:
		#print "EMAIL IS ",emailId
		myList = emailId.split('@')
		myEmail = myList[0]
		email_char = list(myEmail)
		total_char = len(email_char)
		for e in email_char:
			if count == 0:
				new_email.append(e)
				count = count + 1
			elif (total_char == 2) and (count == 1):
				new_email.append("*")
				count = count + 1
			elif total_char == (count + 1):
				new_email.append(e)
				count = count + 1
			else:
				new_email.append("*")
				count = count + 1
		myEmail = "".join(new_email) + "@" + myList[1]
		#print myEmail
	except:
	  print "EXCEPTION IN email encryption ",sys.exc_info()[0]

	return myEmail

