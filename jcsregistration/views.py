from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect #for redirecting Abhishek
from django.template import Context
from django.template.loader import get_template
import logging
import json
# from cities_light.models import City
# from cities_light.models import Region
from django.core.urlresolvers import reverse  # noqa
from django.core.urlresolvers import reverse_lazy  # noqa
from django import shortcuts
from django.template import RequestContext  # noqa
from django.views.generic import TemplateView
from jcsregistration.forms import UserSignupForm
from horizon import forms
from django.conf import settings
from django import http
import esb_Services as esb
import crm_Services as crm
import openstack_dashboard.api.neutron
import openstack_dashboard.api.keystone
import openstack_dashboard.settings as dashboard_settings
import openstack_dashboard.local.local_settings
from django.core.mail import send_mail
from django.core.mail import EmailMessage
import numpy as np
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


import sys

try:
    import simplejson as json
except ImportError:
    import json

LOG = logging.getLogger(__name__)


class UserSignupView(forms.ModalFormView):
    form_class = UserSignupForm
    template_name = "jcsregistration/signup.html"


def index(request):
    return render(request, 'jcsregistration/index.html')

def userValidate(request):
	responseObj = {}
	provisionResponse = {}
	pageUrl = 'jcsregistration/userValidate.html';
	param = "";
	provisionCode = "0"
	provisionMsg = {}
	isOTPSend = False 
	isNetwork = False
	hiddenMail = None
	try:
		customerId = request.GET.get("idcode")
		print "CUSTOMER ID IS ",customerId
		responseObj = esb.getCustomerInfo(customerId)
		if "customerinfo" in responseObj:
			customerInfo = responseObj["customerinfo"] 
			customerName = customerInfo["customername"]
			customerEmail = customerInfo["email"]
			customerContactNo = customerInfo["mobilenumber"]
			phoneValidate = customerInfo["validateflag"]
			organisation = customerInfo["company"]
			#customerPassword = customerInfo["password"] 
			print "Customer NAME ",customerName
			#print "CUSTOMER PASSWORD ",customerPassword
				
			if phoneValidate == 'Y':
				pageUrl = 'jcsregistration/userValidateOTP.html'
				isOTPSend = esb.generateOtp(customerEmail,customerContactNo)
				if isOTPSend:
					provisionMsg["msg"] = "OTP_SUCCESS"
				else:
					provisionMsg["msg"] = "OTP_FAILED"

				provisionMsg["name"] = customerName
				provisionMsg["email"] = customerEmail
				provisionMsg["phone_number"] = customerContactNo
			else:
				provisionResponse = esb.provisionUser(customerName,customerEmail,"0000",customerContactNo)
				provisionCode = provisionResponse["provisionaccountResponse"]["code"]
				
				if provisionCode == "200":
					tenant_id = provisionResponse["provisionaccountResponse"]["tenant_id"]
					authToken = provisionResponse["provisionaccountResponse"]["authToken"]
					isNetwork =  esb.create_network(tenant_id,authToken)
			
					if isNetwork == True:
						param = "SUCCESS"
						hiddenMail = esb.email_encrypt(customerEmail)
					else:
						param = "ERROR"
				else:
					param = "ERROR"
					
				provisionMsg = { "msg" : param, 
						 "regEmail" : hiddenMail,
  					         "login_url": dashboard_settings.DASHBOARD_URL 
                                }
		else:
			param = "ERROR"
			provisionMsg = {"msg" : param}
	
		print "param value ",param 
	except Exception as e:
		print "EXCEPTION OCCURED IN userValidate",e
		param = "ERROR"
		provisionMsg = {"msg" : param}
	
	return render(request, pageUrl,provisionMsg)

def rpHash(person): 
    hash = 5381 
  
    value = person.upper() 
    for caracter in value: 
        hash = (( np.left_shift(hash, 5) + hash) + ord(caracter)) 
    hash = np.int32(hash)
    return hash		 

def registerUser(request):
	responseMsg = ""
	fullName = ""	
	organization = ""
	emailId = ""
	phoneNo = ""
	output = []
	responseObj = ""
	fullInfo = ""
	response_status = 200;
	subject = "I wish to find more details about JioCloud"
	msg = "Contact Me !"
	res = None
	res1 = None
	realPerson = None
	realPersonHash = None
	resolvedHash = None
	try:
		print "In register user..",request.method
		firstName = request.POST.get("firstname")
		print "firstName: ",firstName
		lastName = request.POST.get("lastname")
		print "lastName: ",lastName
		organization = request.POST.get("organization")
		print "organization: ",organization
		emailId = request.POST.get("email_id")
		print "emailId: ",emailId
		phoneNo = request.POST.get("phone_number")
		realPerson = request.POST.get("defaultReal")
		realPersonHash = request.POST.get("gen_hash_num")
		
		print "REAL-PERSON ",realPerson
		print "REAL-PERSON-HASH ",realPersonHash
#		resolvedHash = rpHash(realPerson)
#		print "RESOLVED HASH ",resolvedHash
#		realPersonHash = str(realPersonHash)
#		resolvedHash = str(resolvedHash)
#		print "RESOLVED-PERSON-HASH after using str(): ",realPersonHash
#		print "RESOLVED-HASH after using str():  ",resolvedHash

		fullName = firstName + " "+ lastName
		output.append("Name ")
		output.append(fullName)
		output.append("\n")
		output.append("Organization ")
		output.append(organization)
		output.append("\n")
		output.append("Email Id ")
		output.append(emailId)
		output.append("\n")
		output.append("Phone Number")
		output.append(phoneNo)
		fullInfo = "".join(output)
		print "\n FULL INFO ",fullInfo

#		if (realPersonHash == resolvedHash):
		if (realPersonHash == realPerson):
			print "HASH EQUEAL"
		#	res1 = esb.sendOTP(phoneNo)
		#	print "after sending OTP API response: ",res1
			res = crm.registerUser(fullName,organization,emailId,phoneNo)
			print "resi value after crm services: ",res
                        if res == 'ACCEPTED':
                                response_status = 200
                        else:
                                response_status = 400
		else:
			response_status = 400
			res = 'CAPTCHA'

		responseMsg = { 'code' : res }	
	except:
		print "\nException OCCURED",sys.exc_info()[0]
		responseMsg = { 'code' : 'UNKNOWN_ERROR' }	
		response_status = 400

	finally:
		responseObj = HttpResponse(json.dumps(responseMsg),content_type="text/html")
		print responseObj
		responseObj.status_code = response_status
		print responseObj.status_code
	
	return responseObj

def gotoUserRegister(request):
	pageUrl = 'jcsregistration/early_request.html'
	param = {
		'login_url': dashboard_settings.LOGIN_URL
	}
	return render(request, pageUrl,param)	

# For new Validation Flow
def validationConfirm(request):
	responseObj = {}
	provisionResponse = {}
	pageUrl = 'jcsregistration/restyled/userValidate.html';
	param = "";
	provisionCode = "0"
	provisionMsg = {}
	isOTPSend = False 
	isNetwork = False
	hiddenMail = None
	try:
		customerId = request.GET.get("idcode")
		print "CUSTOMER ID IS ",customerId
			
#		responseObj = esb.getCustomerInfo(customerId)
		responseObj = crm.getCustomerInfo(customerId)
		print "responseObj obtained after calling all the services: ",responseObj
		responseValue  = responseObj['code']
		if responseValue == "SUCCESSFUL":
			hiddenMail = responseObj['mail']			
			param = "SUCCESS"
#		if "customerinfo" in responseObj:
#			customerInfo = responseObj["customerinfo"] 
#			customerName = customerInfo["customername"]
#			customerEmail = customerInfo["email"]
#			customerContactNo = customerInfo["mobilenumber"]
#			phoneValidate = customerInfo["validateflag"]
#			organisation = customerInfo["company"]
#			print "Customer NAME ",customerName
				
#			provisionResponse = esb.provisionUser(customerName,customerEmail,"0000",customerContactNo)
#			provisionCode = provisionResponse["provisionaccountResponse"]["code"]
			
#			if provisionCode == "200":
#				tenant_id = provisionResponse["provisionaccountResponse"]["tenant_id"]
#				authToken = provisionResponse["provisionaccountResponse"]["authToken"]
#				isNetwork =  esb.create_network(tenant_id,authToken)
		
#				if isNetwork == True:
#					param = "SUCCESS"
#					hiddenMail = esb.email_encrypt(customerEmail)
#				else:
#					param = "ERROR"
#			else:
#				param = "ERROR"
				
			provisionMsg = { "msg" : param, 
					 "regEmail" : hiddenMail,
					 "login_url": 'loginPage' 
			}
		else:
			param = "ERROR"
			provisionMsg = {"msg" : param}
	
		print "param value ",param 
	
	except Exception as e:
		print "EXCEPTION OCCURED IN userValidate",e
		param = "ERROR"
		provisionMsg = {"msg" : param}
	
	return render(request, pageUrl,provisionMsg)

def LoginUser(request):
	responseMsg = ""
	username = ""
	password = ""
	responseObj = ""
	output = []
	fullinfo = ""
	response_status = 200;
	userid = ""
	XAuthToken = ""
	reqSystem = "KEYSTONE"
#	pageUrl = 'jcsregistration/restyled/login.html';
#	pageUrl = "restyled/loginPage
	pageUrl = 'loginAbhi'
	res = None
	try:
		print "Invoking ESB service userLogin"
		print "In register User...",request.method
		username = request.POST.get("inputEmail")
		print "username: ",username
		password = request.POST.get("inputPassword")
		print "password: ",password
		output.append(username)
		output.append(password)
		fullinfo = "".join(output)
		print "Full Info: ",fullinfo
		
		res = esb.userLogin(username,password,reqSystem)
		codevalue = res.get('code')
		userid = res.get('user-id')
		XAuthToken = res.get('X-Auth-Token')
#		storing values in cookie - start
		if codevalue == 'LOGIN_SUCCESS':
			request.session["cookievalueXAuthToken"] = XAuthToken  #initializing value for cookie
			request.session["cookievalueuserid"] = userid
			request.session["cookievalueusername"] = username
		else:
			print "not storing any value in cookie as Login_failed"
#		storing vaulues in cookie - end
		print "code value obtained from esb call: ",codevalue
		print "userid value obtained from esb call: ",userid
		print "XAuthToken value obtained from esb call: ",XAuthToken
		if codevalue == 'LOGIN_SUCCESS':
			response_status = 200
#			responseMsg = {'code':'200'}
			res = 'ACCEPTED'
			print "Authenticated"
		elif codevalue == 'LOGIN_FAILED':
			response_status = 400
			res = 'LOGIN_FAILED'
			print "Unauthenticated"			
		else:
			reqSystem = "CRM"
			res = esb.userLogin(username,password,reqSystem)
			codevalue = res.get('code')
			if codevalue == 'LOGIN_SUCCESS':
                	        request.session["cookievalueusername"] = username
	                else:
	                        print "not storing any value in cookie as Login_failed"
			print "code value obtained from esb call: ",codevalue
			if codevalue == 'LOGIN_SUCCESS':
				response_status = 200
        	                res = 'ACCEPTED'
                	        print "Authenticated"
			elif codevalue == 'LOGIN_FAILED':
				response_status = 400
				res = 'LOGIN_FAILED'
				print "UnAuthenticated"
			else:
				response_status = 503
	                        res = 'SERVICE_DOWN'
        	                print "crm service is down"
		responseMsg = { 'code' : res }
		print "responseMsg: ",responseMsg
#		responseMsg = { 'code' : res, 'username': username, 'userid':userid, 'XAuthToken':XAuthToken }
#		responseMsg = { 'code' : res, 'username': username }
	except:
		print "\nException Occurred",sys.exc_info()[0]
		responseMsg = {'code':'UNKNOWN_ERROR'}
		response_status = 400
	finally:
		responseObj = HttpResponse(json.dumps(responseMsg),content_type="text/html")
		print "responseObj: ",responseObj
		responseObj.status_code = response_status
	
	return responseObj
#	return render(request,'/jcs-home',responseMsg)
#	if respose_status == 200:
#		return HttpResponseRedirect("/jcs-home/")
#	else:
#		return redirect('loginAbhi',code= 'LOGIN_FAILED')
#		return HttpResponseRedirect(reverse('restyled/loginPage', kwargs = {'code':'400'}))
#		return redirect(pageUrl,code = '400')
#		return HttpResponseRedirect("/restyled/loginPage/")
#		return render(request,pageUrl,responseMsg)
#		return HttpResponseRedirect(request.META.get('HTTP_REFERER'),responseMsg)
#		return HttpResponseRedirect(pageUrl,responseMsg)

def sendotp(request):
	randomNo = ""
	mobNo = ""
	res = None
	responseMsg = ""
	randomNo = request.POST.get("randomvalue")
	print "randomNo obtained is: ",randomNo
	mobNo = request.POST.get("copyPhoneValue")
	print " the mobile no where otp be sent: ",mobNo
	res = esb.sendOTP(randomNo,mobNo)
	print "res status obtained after sending the gupshup api: ",res
	responseMsg = {'code':'200'}
#	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
	return HttpResponse("OK")

def delloginPageCookie(request):
       print "inside jcsregistration/views.py for deleting cookie"
#       del request.session['cookievalueusername']
#       del request.session['cookievalueuserid']
#       del request.session['cookievalueXAuthToken']
       return render(request, 'jcsregistration/restyled/login.html')

def sendEmail(request):
	print "inside views send email function"
	password = "keo889&&("
	last_name = "ABhishek Korlekar"
	myemail = "abhishek.korlekar@ril.com"
	me = "abhishek.korlekar@ril.com"
	you = myemail
	# Create message container - the correct MIME type is multipart/alternative.
	msg = MIMEMultipart('alternative')
	msg['Subject'] = "Jio Cloud Temporary Password"
	msg['From'] = me
	msg['To'] = you
	# Create the body of the message (a plain-text and an HTML version).
	text = "Dear "+last_name+"\nYour temporary password is "+password+"\nPlease change this password immediately after your first login here: http://demo.jiocloudservices.com/\nEnsure the password is highly secure with a combination of small, Capital, numerical and special characters\n\nThanks,\nJio Cloud Services\n\nDisclaimer: Please do not share your password with any one."
#	html = """\
#	<html>
#	  <head></head>
#	  <body>
#	    <p>Hi!<br>
#	       How are you?<br>
#	       Here is the <a href="http://www.python.org">link</a> you wanted.
#	    </p>
#	  </body>
#	</html>
#	"""
	# Record the MIME types of both parts - text/plain and text/html.
	part1 = MIMEText(text, 'plain')
#	part2 = MIMEText(html, 'html')
	# Attach parts into message container.
	# According to RFC 2046, the last part of a multipart message, in this case
	# the HTML message, is best and preferred.
	msg.attach(part1)
#	msg.attach(part2)
	# Send the message via local SMTP server.
	s = smtplib.SMTP('10.137.2.24', 25)
	# sendmail function takes 3 arguments: sender's address, recipient's address
	# and message to send - here it is sent as one string.
	s.sendmail(me, you, msg.as_string())
	s.quit()
	print "success"
	return render(request, 'jcsregistration/restyled/login.html')
