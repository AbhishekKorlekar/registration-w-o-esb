# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2013 Reliance Jio Infocomm, Ltd.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import logging
import esb_Services as esb
# from captcha.fields import ReCaptchaField
# from cities_light.models import City
# from cities_light.models import Region
# from cities_light.models import Country
from django.core.urlresolvers import reverse_lazy  # noqa
from django.utils.translation import ugettext_lazy as _  # noqa
from django.views.decorators.debug import sensitive_variables  # noqa
from django.forms import ValidationError  # noqa
from horizon import forms
from horizon.utils import validators
from keystoneclient import exceptions as keystoneclient_exceptions
from django.conf import settings
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse

try:
    import simplejson as json
except ImportError:
    import json

LOG = logging.getLogger(__name__)

class UserSignupForm(forms.SelfHandlingForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Name'}))
    organisation = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Organisation'}))
    # last_name = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Last Name'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Your Email'}))
    # confirm_email = forms.EmailField()
    '''	
    password = forms.RegexField(
        label=_("Password"),
        widget=forms.PasswordInput(render_value=False, attrs={'placeholder': 'New Password'}),
        regex=validators.password_validator(),
        error_messages={'invalid': validators.password_validator_msg()})
    '''
    # confirm_password = forms.CharField(
    #     label=_("Confirm Password"),
    #     required=False,
    #     widget=forms.PasswordInput(render_value=False))

    # company = forms.CharField()
    # address = forms.CharField()
    # country = forms.ChoiceField(label=_("Country"), required=True)
    # state = forms.ChoiceField(label=_("State"), required=True)
    # city = forms.ChoiceField(label=_("City"), required=True)
    # pincode = forms.CharField()
    # country_code = forms.CharField()
    #phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Your Phone Number'}))
    phone = forms.RegexField(widget=forms.TextInput(attrs={'placeholder':'Phone Number +91'}),
                            regex=r'^\d{10,10}$', 
                            error_message = ("Phone number must be entered in the format: '+999999999'. Up to 10 digits allowed."))
    '''
    captcha = ReCaptchaField(
    public_key='6Lf5kwgTAAAAALCBck1OgP8byI1FjvB_Kb2cYCj2',
    private_key='6Lf5kwgTAAAAAI3x5texTAaUVLPZJFIxO5pHlGPf',
    use_ssl=True
    )
    '''
    #captcha = ReCaptchaField()
    # terms = forms.BooleanField(
    #     error_messages={
    #         'required': 'You must accept the terms and conditions'},
    #     label="Terms &amp; Conditions"
    # )

    def __init__(self, request, *args, **kwargs):
        super(UserSignupForm, self).__init__(request, *args, **kwargs)
        # self.fields["country"].choices = [(c.id, c.name_ascii) for c in Country.objects.all()]
        # if request.method == "GET":
        #     default_country = self.get_default_country()
        #     if default_country:
        #         self.fields["country"].initial = default_country.id
        #         self.fields["state"].choices = [(s.id, s.name_ascii) for s in Region.objects.filter(country_id=default_country.id).all()]
        #         default_state= self.get_default_state()
        #         if default_state:
        #             self.fields["state"].initial = default_state.id
        #             self.fields["city"].choices = [(ci.id, ci.name_ascii) for ci in City.objects.filter(region_id=default_state.id).all()]
        #             default_city= self.get_default_city()
        #             if default_city:
        #                 self.fields["city"].initial = default_city.id
        # elif request.method == "POST":
        #     country_id = request.POST.get("country")
        #     if country_id:
        #         self.fields["country"].initial = country_id
        #         self.fields["state"].choices = [(s.id, s.name_ascii) for s in Region.objects.filter(country_id=country_id).all()]
        #         state_id = request.POST.get("state")
        #         if state_id:
        #             self.fields["state"].initial = state_id
        #             self.fields["city"].choices = [(ci.id, ci.name_ascii) for ci in City.objects.filter(region_id=state_id).all()]
        #             city_id = request.POST.get("city")
        #             if city_id:
        #                 self.fields["city"].initial = city_id

    
    def clean(self):
        '''Check to make sure password fields match.'''
        data = super(forms.Form, self).clean()
        # if 'password' in data:
        #     if data['password'] != data.get('confirm_password', None):
        #         raise ValidationError(_('Passwords do not match.'))
        # if 'email' in data:
        #     if data['email'] != data.get('confirm_email', None):
        #         raise ValidationError(_('Emails do not match.'))
        return data

    @sensitive_variables('data')
    def handle(self, request, data):
        res = {}
        user_id = None
       # password = data.get("password")
        email = data.get("email")
        first_name = data.get("first_name")
        # last_name = data.get("last_name")
        # company = data.get("company")
        # address = data.get("address")
        # country_code = data.get("country_code")
        # country = data.get("country")
        # country_list = self.get_country(country)
        # if country_list:
        #     country_name = country_list[0].name_ascii
        # state = data.get("state")
        # state_list = self.get_state(state)
        # if state_list:
        #     state_name = state_list[0].name_ascii
        # city = data.get("city")
        # city_list = self.get_city(city)
        # if city_list:
        #     city_name = city_list[0].name_ascii
        # pin = data.get("pincode")
        phone = data.get("phone")
        organisation = data.get("organisation")
	subject = "I wish to find more details about JioCloud"
	msg = "Contact Me !"
	responseObj = esb.createLead(first_name,subject,email,phone,msg,organisation) 
	
	msg = {
		"msg" : responseObj
	}
	return HttpResponse(json.dumps(msg),content_type="application/json") 
        #return render_to_response('/jcsregistration/signup')
        #return render_to_response('registers/success.html')


