#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import horizon


from django.conf import settings
from django.core.urlresolvers import reverse
from django.template import defaultfilters as filters
from django.utils.translation import npgettext_lazy
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext_lazy
from horizon import exceptions
from horizon import forms
from horizon import tables
from horizon.utils import functions
from openstack_dashboard import api
from openstack_dashboard.api import base
from openstack_dashboard.api import nova
from openstack_dashboard.dashboards.project.containers \
    import browsers as jcsbrowserviews
from openstack_dashboard.dashboards.project.containers \
    import tables as jcscontainertable
from openstack_dashboard.dashboards.project.images.images \
    import tables as jcsimgtable
from openstack_dashboard.dashboards.project.images.images \
    import tables as jcsimage_imagetables
from openstack_dashboard.dashboards.project.images \
    import utils as image_utils
from openstack_dashboard.dashboards.project.images \
    import views as jcsimgviews
from openstack_dashboard.dashboards.project.images \
    import views as jcsimage_imageviews
from openstack_dashboard.dashboards.project.instances \
    import tables as jcsinstancetable
from openstack_dashboard.dashboards.project.instances \
    import views as jcsinstanceviews
from openstack_dashboard.dashboards.project.instances \
    import views as jcscreate_instanceviews
from openstack_dashboard.dashboards.project.instances.workflows \
    import create_instance as jcscreate_instance
from openstack_dashboard.dashboards.project.networks \
    import tables as jcsnetworktables
from openstack_dashboard.dashboards.project.networks \
    import views as jcsnetworkviews
from openstack_dashboard.dashboards.project.volumes.backups \
    import tables as jcsvolume_backupstables
from openstack_dashboard.dashboards.project.volumes \
    import tabs as jcsvolumetabs
from openstack_dashboard.dashboards.project.volumes \
    import views as jcsvolumeviews
from openstack_dashboard.dashboards.project.volumes.volumes \
    import forms as jcsvolumeforms
from openstack_dashboard.dashboards.project.volumes.volumes \
    import tables as jcsvolume_volumetables
from openstack_dashboard.dashboards.project.volumes.volumes \
    import views as jcsvolume_volumeviews
from openstack_dashboard.usage import quotas

import logging
KEYPAIR_IMPORT_URL = "horizon:project:access_and_security:keypairs:import"
LOG = logging.getLogger(__name__)


class JCSImagesTable(jcsimgtable.ImagesTable):
    class Meta(jcsimgtable.ImagesTable.Meta):
        table_actions = (jcsimgtable.OwnerFilter,)
        launch_actions = ()
        if getattr(settings, 'LAUNCH_INSTANCE_LEGACY_ENABLED', True):
            launch_actions = (jcsimgtable.LaunchImage,) + launch_actions
        if getattr(settings, 'LAUNCH_INSTANCE_NG_ENABLED', False):
            launch_actions = (jcsimgtable.LaunchImageNG,) + launch_actions
        row_actions = launch_actions + (
            jcsimgtable.CreateVolumeFromImage,
            jcsimgtable.EditImage,
        )
jcsimgviews.IndexView.table_class = JCSImagesTable


class JCSContainerTable(jcscontainertable.ContainersTable):
    class Meta(jcscontainertable.ContainersTable.Meta):
        row_actions = (
            jcscontainertable.ViewContainer,
            jcscontainertable.DeleteContainer,
        )
jcsbrowserviews.ContainerBrowser.navigation_table_class = JCSContainerTable


class JCSStopInstance(jcsinstancetable.StopInstance):
    @staticmethod
    def action_present(count):
        return npgettext_lazy(
            "Action to perform (the instance is currently running)",
            u"Stop Instance",
            u"Stop Instances",
            count
        )

    @staticmethod
    def action_past(count):
        return npgettext_lazy(
            "Past action (the instance is currently already Shut Off)",
            u"Stop Off Instance",
            u"Stop Off Instances",
            count
        )


class JCSLaunchLink(jcsinstancetable.LaunchLink):
        verbose_name = _("Launch New Instance")


class JCSInstanceTable(jcsinstancetable.InstancesTable):
        az = tables.Column(
            "availability_zone",
            verbose_name=_("Availability Zone"),
            hidden=True
        )

        class Meta(jcsinstancetable.InstancesTable.Meta):
            table_actions_menu = (
                jcsinstancetable.StartInstance,
                JCSStopInstance,
                jcsinstancetable.SoftRebootInstance
            )
            row_actions = (
                jcsinstancetable.StartInstance,
                jcsinstancetable.SimpleAssociateIP,
                jcsinstancetable.AssociateIP,
                jcsinstancetable.SimpleDisassociateIP,
                jcsinstancetable.EditInstance,
                jcsinstancetable.DecryptInstancePassword,
                jcsinstancetable.EditInstanceSecurityGroups,
                jcsinstancetable.ConsoleLink,
                jcsinstancetable.LogLink,
                jcsinstancetable.TogglePause,
                jcsinstancetable.ToggleSuspend,
                jcsinstancetable.SoftRebootInstance,
                jcsinstancetable.RebootInstance,
                JCSStopInstance, jcsinstancetable.RebuildInstance,
                jcsinstancetable.TerminateInstance
            )
            launch_actions = ()
            if getattr(settings, 'LAUNCH_INSTANCE_LEGACY_ENABLED', True):
                launch_actions = (JCSLaunchLink,) + launch_actions
            if getattr(settings, 'LAUNCH_INSTANCE_NG_ENABLED', False):
                launch_actions = (JCSLaunchLink,) + launch_actions
jcsinstanceviews.IndexView.table_class = JCSInstanceTable


class JCSSetInstanceDetailsAction(jcscreate_instance.SetInstanceDetailsAction):
    availability_zone = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )
    name = forms.CharField(
        label=_("Instance Name"),
        initial='Server',
        max_length=255
    )
    volume_size = forms.IntegerField(
        label=_("Device size (GB)"),
        initial=8,
        min_value=5,
        required=False,
        help_text=_("Volume size in gigabytes ""(integer value).")
    )
    delete_on_terminate = forms.BooleanField(
        label=_("Delete on Terminate"),
        initial=True,
        required=False,
        help_text=_("Delete volume on ""instance terminate")
    )

    class Meta(object):
        name = _("Details")
        help_text_template = ("project/instances/""_launch_details_help.html")

    #  def clean(self):
    #  cleaned_data = super(JCSSetInstanceDetailsAction, self).clean()

    def __init__(self, request, context, *args, **kwargs):
        self._init_images_cache()
        self.request = request
        self.context = context
        super(JCSSetInstanceDetailsAction, self).__init__(
            request, context, *args, **kwargs)
        #  Hide the device field if the hypervisor doesn't support it.
        if not nova.can_set_mount_point():
            self.fields['device_name'].widget = forms.widgets.HiddenInput()

        source_type_choices = []
        if base.is_service_enabled(request, 'volume'):
            try:
                if api.nova.extension_supported("BlockDeviceMappingV2Boot",
                                                request):
                    source_type_choices.append(
                        ("volume_image_id",
                         _("Boot from image"
                             " (creates a new volume from image)"))
                    )
            except Exception:
                exceptions.handle(request, _('Unable to retrieve extensions '
                                             'information.'))

            source_type_choices.append(
                ("volume_snapshot_id",
                 _("Boot from volume snapshot (creates a new volume)")))
        self.fields['source_type'].choices = source_type_choices

    def populate_image_id_choices(self, request, context):
        choices = []
        images = image_utils.get_available_images(request,
                                                  context.get('project_id'),
                                                  self._images_cache)
        for image in images:
            image.bytes = image.virtual_size or image.size
            image.volume_size = max(
                image.min_disk, functions.bytes_to_gigabytes(image.bytes))
            choices.append((image.id, image))
            if context.get('image_id') == image.id and \
                    'volume_size' not in context:
                context['volume_size'] = image.volume_size
        if choices:
            choices.sort(key=lambda c: c[1].name)
        else:
            choices.insert(0, ("", _("No images available")))
        return choices


class JCSSetInstanceDetails(jcscreate_instance.SetInstanceDetails):
    action_class = JCSSetInstanceDetailsAction
# volumes table details section

jcsvolumetabs.BackupsTab.name = _("Snapshots")
jcsvolume_volumetables.CreateBackup.verbose_name = _("Create Snapshot")


class JCSCreateBackup(jcsvolume_volumetables.CreateBackup):
    verbose_name = _("Create Snapshot")


class JCSDeleteBackup(jcsvolume_backupstables.DeleteBackup):
    data_type_singular = _("Volume Snapshot")
    data_type_plural = _("Volume Snapshots")


class JCSRestoreBackup(jcsvolume_backupstables.RestoreBackup):
    verbose_name = _("Restore Snapshot")


class JCSBackupsTable(jcsvolume_backupstables.BackupsTable):
    class Meta(object):
        verbose_name = _("Volume Snapshots")
        table_actions = (JCSDeleteBackup,)
        row_actions = (JCSRestoreBackup, JCSDeleteBackup)


class JCSVolumesTable(jcsvolume_volumetables.VolumesTable):
    name = tables.Column(
        "name",
        verbose_name=_("Name"),
        link="horizon:project:volumes:volumes:detail"
    )
    attachments = jcsvolume_volumetables.AttachmentColumn(
        "attachments",
        verbose_name=_("Attached To")
    )
    availability_zone = tables.Column("availability_zone",
                                      verbose_name=_("Availability Zone"),
                                      hidden=True)
    bootable = tables.Column('is_bootable',
                             verbose_name=_("Bootable"),
                             filters=(filters.yesno, filters.capfirst),
                             hidden=True)
    encryption = tables.Column(jcsvolume_volumetables.get_encrypted_value,
                               verbose_name=_("Encrypted"),
                               link="horizon:project:volumes:"
                                    "volumes:encryption_detail",
                                    hidden=True)

    class Meta(object):
        name = "volumes"
        verbose_name = _("Volumes")
        status_columns = ["status"]
        row_class = jcsvolume_volumetables.UpdateRow
        table_actions = (
            jcsvolume_volumetables.CreateVolume,
            jcsvolume_volumetables.DeleteVolume,
            jcsvolume_volumetables.VolumesFilterAction,
        )
        row_actions = (
            jcsvolume_volumetables.EditVolume,
            jcsvolume_volumetables.ExtendVolume,
            jcsvolume_volumetables.EditAttachments,
            jcsvolume_volumetables.DeleteVolume,
            JCSCreateBackup,
        )
jcsvolumetabs.VolumeTab.table_classes = (JCSVolumesTable,)
# Volumes Section


class JCSVolumeAndSnapshotTabs(jcsvolumetabs.VolumeAndSnapshotTabs):
    slug = "volumes_and_snapshots"
    tabs = (jcsvolumetabs.VolumeTab, jcsvolumetabs.BackupsTab)
    sticky = True

jcsvolumeviews.IndexView.tab_group_class = JCSVolumeAndSnapshotTabs
# Volumes form section


class JCSVolumeCreateForm(jcsvolumeforms.CreateForm):
    availability_zone = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )
    type = forms.CharField(
        widget=forms.HiddenInput(),
        required=False,
    )

jcsvolume_volumeviews.CreateView.form_class = JCSVolumeCreateForm
# ACCESS CONTROL FUNCTION


class JCSSetAccessControlsAction(jcscreate_instance.SetAccessControlsAction):
        keypair = forms.DynamicChoiceField(
            label=_("Key Pair"),
            required=True,
            help_text=_("Key pair to use for ""authentication."),
            add_item_link=KEYPAIR_IMPORT_URL)

        class Meta(object):
            name = _("Access & Security")
            help_text = _("Control access to your instance via key pairs, "
                          "security groups, and other mechanisms.")


class JCSSetAccessControls(jcscreate_instance.SetAccessControls):
    action_class = JCSSetAccessControlsAction
# Launch Instance workflow for above two tabs


class JCSLaunchInstance(jcscreate_instance.LaunchInstance):
    default_steps = (
        jcscreate_instance.SelectProjectUser,
        JCSSetInstanceDetails,
        JCSSetAccessControls,
        jcscreate_instance.SetNetwork,
        jcscreate_instance.PostCreationStep,
        jcscreate_instance.SetAdvanced
    )
jcscreate_instanceviews.LaunchInstanceView.workflow_class = JCSLaunchInstance
# networks renames


class JCSCreateNetwork(jcsnetworktables.CreateNetwork):
    name = "create"
    verbose_name = _("Create VPC")
    url = "horizon:project:networks:create"
    classes = ("ajax-modal",)
    icon = "plus"
    policy_rules = (("network", "create_network"),)

    def allowed(self, request, datum=None):
        usages = quotas.tenant_quota_usages(request)
        if usages['networks']['available'] <= 0:
            if "disabled" not in self.classes:
                self.classes = [c for c in self.classes] + ["disabled"]
                self.verbose_name = _("Create VPC (Quota exceeded)")
        else:
            self.verbose_name = _("Create VPC")
            self.classes = [c for c in self.classes if c != "disabled"]

        return True


class JCSDeleteNetwork(jcsnetworktables.DeleteNetwork):
    @staticmethod
    def action_present(count):
        return ungettext_lazy(
            u"Delete VPC",
            u"Delete VPCs",
            count
        )

    @staticmethod
    def action_past(count):
        return ungettext_lazy(
            u"Deleted VPC",
            u"Deleted VPCs",
            count
        )

        def delete(self, request, network_id):
            network_name = network_id
            try:
                # Retrieve the network list.
                network = api.neutron.network_get(
                    request,
                    network_id,
                    expand_subnet=False
                )
                network_name = network.name
                LOG.debug('VPC %(network_id)s has subnets: %(subnets)s',
                          {'network_id': network_id,
                           'subnets': network.subnets})
                for subnet_id in network.subnets:
                    api.neutron.subnet_delete(request, subnet_id)
                    LOG.debug('Deleted subnet %s', subnet_id)
                api.neutron.network_delete(request, network_id)
                LOG.debug('Deleted VPC %s successfully', network_id)
            except Exception:
                msg = _('Failed to delete VPC %s')
                LOG.info(msg, network_id)
                redirect = reverse("horizon:project:networks:index")
                exceptions.handle(
                    request,
                    msg % network_name,
                    redirect=redirect
                )


class JCSEditNetwork(jcsnetworktables.EditNetwork):
    name = "update"
    verbose_name = _("Edit VPC")


class JCSNetworksTable(jcsnetworktables.NetworksTable):
        class Meta(jcsnetworktables.NetworksTable.Meta):
            name = "networks"
            verbose_name = _("VPCs")
            table_actions = (
                jcsnetworktables.CreateNetwork,
                jcsnetworktables.DeleteNetwork,
                jcsnetworktables.NetworksFilterAction
            )
            row_actions = (
                JCSEditNetwork,
                jcsnetworktables.CreateSubnet,
                JCSDeleteNetwork
            )
jcsnetworkviews.IndexView.table_class = JCSNetworksTable
jcsnetworkviews.IndexView.page_title = _("VPCs")
# Images Section


class JCSImagesTable(jcsimage_imagetables.ImagesTable):
    class Meta(jcsimage_imagetables.ImagesTable.Meta):
        launch_actions = ()
        if getattr(settings, 'LAUNCH_INSTANCE_LEGACY_ENABLED', True):
            launch_actions = (
                jcsimage_imagetables.LaunchImage,
            ) + launch_actions
        if getattr(settings, 'LAUNCH_INSTANCE_NG_ENABLED', False):
            launch_actions = (
                jcsimage_imagetables.LaunchImageNG,
            ) + launch_actions
        row_actions = launch_actions + (
            jcsimage_imagetables.CreateVolumeFromImage,
        )
jcsimage_imageviews.IndexView.table_class = JCSImagesTable


project_db = horizon.get_dashboard('project')
project_db.name = _("My Resources")
project_db.default_panel = "instances"
compute_panel_group = project_db.get_panel_group("compute")
compute_panel_group.name = _("JCS Compute")
overview_panel = project_db.get_panel("overview")
project_db.unregister(overview_panel.__class__)
router_panel = project_db.get_panel("routers")
project_db.unregister(router_panel.__class__)
loadbalancers_panel = project_db.get_panel("loadbalancers")
project_db.unregister(loadbalancers_panel.__class__)
vpn_panel = project_db.get_panel("vpn")
project_db.unregister(vpn_panel.__class__)
firewall_panel = project_db.get_panel("firewalls")
project_db.unregister(firewall_panel.__class__)
network_panel = project_db.get_panel("networks")
network_panel.name = _("VPCs")
network_panel_topology = project_db.get_panel("network_topology")
network_panel_topology.name = _("VPC Topology")
network_panel_group = project_db.get_panel_group("network")
network_panel_group.name = _("JCS Virtual Private Cloud")
object_panel_group = project_db.get_panel_group("object_store")
object_panel_group.name = _("JCS Objects")
#  visible changes done. Changes from 10.140.216.22 started
orchestration_panel_group = project_db.get_panel_group("orchestration")
orchestration_panel_group.name = _("JCS Orchestration")
databases_panel_group = project_db.get_panel_group("database")
databases_panel_group.name = _("JCS Databases")
